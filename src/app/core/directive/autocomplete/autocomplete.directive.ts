import { Directive, EventEmitter } from '@angular/core';
import { AutoComplete } from 'primeng/autocomplete';

@Directive({
    selector: '[horus-autocomplete]'
})
export class HorusAutocompleteDirective {

    eventEmitter: any = new EventEmitter<any>(true);

    constructor(
        private autocomplete: AutoComplete
    ) {
        this.autocomplete.completeMethod.emit({value: 'TESTE'});
    }
}
