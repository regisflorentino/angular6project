import { Injector, Injectable, OnInit, Input } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { FormBuilder } from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";
import { HorusTranslationLoaderService, Locale } from "app/core/service/translation-loader.service";
import { AppModule } from "app/app.module";
import { ValidationForm } from "app/core/forms/validation.form";

@Injectable()
export abstract class HorusFormPage implements OnInit {

    protected translate: TranslateService;
    protected fuseTranslationLoader: HorusTranslationLoaderService;
    
    protected validationForm: ValidationForm;
    public id: number;

    constructor(protected injector: Injector, protected activatedRoute: ActivatedRoute, private args: Locale[]) {
        this.translate = this.injector.get(TranslateService);
        this.fuseTranslationLoader = this.injector.get(HorusTranslationLoaderService);

        this.translate.use('pt');
        this.activatedRoute.params.subscribe(params => {
            this.id = params['parameters'];
        });
        this.fuseTranslationLoader.loadTranslations(this.args);
        this.validationForm = new ValidationForm(this.injector.get(FormBuilder));

    }

    ngOnInit(): void {
    }


}