import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HorusTemplateModule } from 'app/main/template/template.module';
import { ComponentsModule } from 'app/core/components/components.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PrimefacesModule } from 'app/core/components/primefaces.module';
import { DirectiveModule } from 'app/core/directive/diretive.module';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from 'app/core/components/material.module';
import { TabBaseDadosWorkflowFormComponent } from 'app/main/horus/administrador/pages/ADMM16/form/formWorkflow/tabs/baseDeDados/tab.base.de.dados';
import { TabAcionamentoWorkflowFormComponent } from 'app/main/horus/administrador/pages/ADMM16/form/formWorkflow/tabs/acionamento/tab.acionamento';



@NgModule({
    declarations: [
        TabBaseDadosWorkflowFormComponent,
        TabAcionamentoWorkflowFormComponent
    ],
    imports: [
        HorusTemplateModule,
        FlexLayoutModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PrimefacesModule,
        ComponentsModule,
        DirectiveModule,
        TranslateModule,
        MaterialModule

    ],
    exports: [
        TabBaseDadosWorkflowFormComponent,
        TabAcionamentoWorkflowFormComponent
    ]

})
export class ManutencaoWorkflowTabsModule {
}
