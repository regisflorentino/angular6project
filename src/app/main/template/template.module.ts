import { NgModule } from '@angular/core';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { LeftNavTemplate } from './left-menu/left.nav.menu.template';
import { MenuModule } from '../side-menu/side.menu.module';
import { ToolbarModule } from '../toolbar/toolbar.module';
import { BreadCrumbComponent } from '../../core/components/breadcrumb/breadcrumb.component';
import { ComponentsModule } from '../../core/components/components.module';
import { MaterialModule } from '../../core/components/material.module';
import { HorusFormPage } from './typescript/form.page';
import { FormsModule } from '@angular/forms';

@NgModule({
    
    declarations: [
        LeftNavTemplate,
    ],

    imports: [
        MenuModule,
        ToolbarModule,
        ComponentsModule,
        MaterialModule,
        FormsModule
    ],

    exports: [
        LeftNavTemplate
    ]
})
export class HorusTemplateModule { }
