import { AcionamentoWorkflowModel } from "./acionamento.model";

export interface WorkflowModel {

    id                  : number;
    programa            : number;
    programaListagem    : number;
    prazoValidade       : number;

    nome                : string;
    comparador          : 'AND' | 'OR';
    
    status              : boolean;
    acionamentos        : Array<AcionamentoWorkflowModel>;
}