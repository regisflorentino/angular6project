import { TestBed, async } from '@angular/core/testing';
import { ManutencaoWorkflowFormComponent } from './manutencao.workflow.form';
import { ComponentsModule } from '../../../../../../../core/components/components.module';
import { PrimefacesModule } from 'app/core/components/primefaces.module';
import { ManutencaoWorkflowModule } from 'app/main/horus/administrador/pages/ADMM16/manutencao.workflow.module';
import { ValidationForm } from '../../../../../../../core/forms/validation.form';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { ManutencaoWorkflowTabsModule } from 'app/main/horus/administrador/pages/ADMM16/form/formWorkflow/tabs/manutencao.workflow.tabs.module';
import { RouterModule, ActivatedRoute } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule, APP_BASE_HREF } from '@angular/common';
import { DirectiveModule } from 'app/core/directive/diretive.module';
import { MaterialModule } from 'app/core/components/material.module';
import { ClientModule } from 'app/client/horus/horus.client.module';
import { HorusTemplateModule } from 'app/main/template/template.module';
import { TranslateModule } from '@ngx-translate/core';
import { HorusTranslationLoaderService } from 'app/core/service/translation-loader.service';
import { CookieService } from 'ngx-cookie-service';
import { MockCookieService } from 'test/mock/service/cookie.service.mock';
import { MenuClient } from '../../../../../../../client/horus/seguranca/usuario/menu.client';
import { environment as env } from 'environments/environment';
import { environment as testEvn } from 'environments/environment.test';
import { HttpModule } from '@angular/http';
import { AutenticacaoClient } from 'app/client/horus/seguranca/oauth/autenticacao.client';
import { LoginComponent } from 'app/main/horus/root/login/login.component';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { FakeDbService } from 'test/mock-db/mock.db.service';
import { environment } from 'environments/environment';
import { HorusClientResource } from 'app/client/horus/horus.client.resource';
import { HorusClientResourceTest } from 'test/client/client.resource.test';

describe('ManutencaoWorkflowFormComponent', () => {

  beforeEach(async(() => {

    TestBed.configureTestingModule({

      declarations: [
        ManutencaoWorkflowFormComponent,
      ],

      providers: [
        { provide: APP_BASE_HREF, useValue: '/ADM/ADMM16' },
        { provide: HorusClientResource, useValue: new HorusClientResourceTest() },
        

        CookieService,
      ],

      imports: [
        HttpModule,
        ManutencaoWorkflowTabsModule,
        RouterModule.forRoot([]),
        InMemoryWebApiModule.forRoot(FakeDbService, {
          delay             : 0,
          passThruUnknownUrl: true
        }),
        HorusTemplateModule,
        FlexLayoutModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PrimefacesModule,
        ComponentsModule,
        DirectiveModule,
        MaterialModule,
        ClientModule,
        TranslateModule.forRoot({}),

      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(ManutencaoWorkflowFormComponent);
    const form: ManutencaoWorkflowFormComponent = fixture.debugElement.componentInstance;
    expect(form).toBeTruthy();
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(ManutencaoWorkflowFormComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    
    //expect(compiled.querySelector('modulo').textContent).toContain('Welcome to horus-angular-components!');
  }));

/*
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app');
  }));

*/

});
