import { NgModule } from '@angular/core';

import { BreadCrumbComponent } from './breadcrumb/breadcrumb.component';
import { AlertComponent } from './alert/alert.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { HorizontalFormComponent } from 'app/core/components/field/horizontal/horizontal.field.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material.module';
import { TooltipModule } from 'ngx-tooltip';
import { TranslateModule } from '@ngx-translate/core';
import { HorusTranslationLoaderService } from 'app/core/service/translation-loader.service';
import { PrimefacesModule } from 'app/core/components/primefaces.module';
import { CookieService } from 'ngx-cookie-service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HorusDatatableModule } from './datatable/datatable.module';

@NgModule({

    providers: [
        HorusTranslationLoaderService,
        CookieService,
    ],

    declarations: [
        BreadCrumbComponent,
        AlertComponent,
        HorizontalFormComponent,
    ],

    imports: [
        AngularFontAwesomeModule,
        FlexLayoutModule,
        CommonModule,
        ReactiveFormsModule,
        MaterialModule,
        TooltipModule,
        TranslateModule,
        HorusDatatableModule
        
    ],

    exports: [
        BreadCrumbComponent,
        AlertComponent,
        HorizontalFormComponent,
        AngularFontAwesomeModule,
        TooltipModule,
        TranslateModule,
        PrimefacesModule,
        HorusDatatableModule
    ],
})
export class ComponentsModule { }
