base.masked = function(){
	var object = {};
	object.initElement = function(element){
		element.mask(element.attr("data-mascara"));
	}
	
	object.init = function(){
		$(".base-input-mascara").each(function(index, obj){
			object.initElement($(obj));
		});
		
	};
	
	return object;
}
