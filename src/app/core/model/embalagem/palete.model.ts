export class Palete {

    altura  : number;
	lastro  : number;
	
	public getCubagem() : number{
		let cubagem = null;
		if(this.altura && this.altura > 0 && this.lastro && this.lastro > 0){
			cubagem = this.lastro * this.altura;
		}
		return cubagem;
	}
}