base.breadcrumb = function(){
	var object = {};
	
	var breadcrumbOptional = function(){		
		var name = $("#breadcrumbsOptional").attr("data-name");
		var icon = $("#breadcrumbsOptional").attr("data-icon");
		var link = $("#breadcrumbsOptional").attr('data-link');

		if(link) {
			$(".breadcrumb li:first-child a").attr("href", link);
		}
		
		$(".breadcrumb li:first-child a").text(name);
		$(".breadcrumb li:first-child i").removeClass().addClass(icon);
	};
	
	object.init = function(){
		$(window).ready(function(){
			breadcrumbOptional();
		})
	};
	
	return object;
};