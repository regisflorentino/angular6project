import { Component, ViewEncapsulation, OnInit, Input, Injector } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { HorusTranslationLoaderService, Locale } from "app/core/service/translation-loader.service";

import { locale as portugues } from '../../../i18n/pt';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { horusAnimation } from "app/core/animations";
import { DadosGeraisModel } from "app/core/model/produto/dadosgerais.model";
import { Fabricante } from "app/core/model/fabricante/fabricante.model";
import { ActivatedRoute } from "@angular/router";
import { HorusFormPage } from "../../../../../../../template/typescript/form.page";
import { SelectItem } from "primeng/api";
import { FornecedorProdutoModel } from "../../../../../../../../core/model/produto/fornecedor.produto.model";
import { PaginacaoHorus } from "app/core/components/horus-datatable/domain/paginacaoHorus";
import { ValidationForm } from "../../../../../../../../core/forms/validation.form";
import { FornecedorProdutoClient } from "app/client/horus/produto/fornecedor/fornecedor.client";
import { WorkflowProcessorClient } from "app/client/horus/workflow/workflow.client";

@Component({
    selector: 'manutencao-produto-fornecedores',
    templateUrl: './fornecedores.tab.html',
    styleUrls: ['./fornecedores.tab.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: horusAnimation
})
export class ManutencaoProdutoFornecedoresComponent extends HorusFormPage implements OnInit {

    codigo: number;
    model: FornecedorProdutoModel;

    fornecedores: Array<FornecedorProdutoModel>;

    constructor(private workflowClient: WorkflowProcessorClient, private client: FornecedorProdutoClient, protected activatedRoute: ActivatedRoute, protected injector: Injector){
        super(injector, activatedRoute, [portugues]);
    }

    compare(event){
        event.preventDefault();
        this.fornecedores[0].ativo = false;
        this.fornecedores[1].ativo = false;
        this.fornecedores[2].ativo = false;
        
        this.client.load(this.id, 'FORM').subscribe(
            success => {
                this.workflowClient.compareInstanceValues(success, this.fornecedores).subscribe(
                    success => {
                        console.log(success);
                    }
                );
            }
        );

    }

    ngOnInit(): void {
        this.client.load(this.id, 'FORM').subscribe(
            success => {
                this.fornecedores = success;
            }, 
            error => {
                this.fornecedores = new Array<FornecedorProdutoModel>();
            }
        );

        this.validationForm.init({});
        this.validationForm.buildForm({});
        this.validationForm.valuesChanges();

    }

}