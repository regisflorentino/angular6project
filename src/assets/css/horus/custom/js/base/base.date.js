base.date = function(){
	var object = {};
	
	object.initElement = function(element){
		var startDate = element.find('input[type="text"]').attr('data-min-date');
		var endDate = element.find('input[type="text"]').attr('data-max-date');
		var readonly = element.find('input[type="text"]').attr('readonly');
		if(!readonly){
			element.find(".date-picker").datepicker({
				startDate: startDate,
				endDate: endDate,
				format: 'dd/mm/yyyy',
				language: 'pt-BR',
				autoclose: true, 
				todayHighlight: true
			});
		}
	};
	
	object.init = function(){
		$(".base-datepicker").each(function(index, obj){
			object.initElement($(obj));
		});
	}
	
	return object;
};