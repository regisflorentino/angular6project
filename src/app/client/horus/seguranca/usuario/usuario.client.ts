import { EndPoint } from "../../../../core/client/endpoint.server";
import { environment } from "environments/environment";
import { HorusClient } from "../../horus.client";
import { Token } from "app/core/model/oauth/token.model";
import { Injectable } from "@angular/core";
import { ClientResource } from "app/core/client/client.resource";
import { Observable } from "rxjs";
import { RequestOptions, URLSearchParams } from "@angular/http";
import { map, catchError } from 'rxjs/operators';
import { RefreshTokenResponse } from "app/core/model/oauth/refresh.toke.model";
import { UsuarioModel } from "../../../../core/model/usuario/usuario.model";
import { MenuItem } from "primeng/api";

@Injectable()
@ClientResource({
    path: '/usuario',
    endPoint: environment.serviceEndPoints.seguranca
})
export class UsuarioClient extends HorusClient<UsuarioModel>{
    
    findUsuarioLogado() : Observable<UsuarioModel>{
        return this.get({
            path: '/findUsuarioLogado',
        });
    }

}
