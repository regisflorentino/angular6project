export interface Fabricante {
    id      : number;
    nome    : string;
    status  : boolean;
}