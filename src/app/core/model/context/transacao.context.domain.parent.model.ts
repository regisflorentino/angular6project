export interface TransacaoContextDomainParentComponent {
    transacaoId         : number;
    usuarioCadastroId   : number;
    usuarioAlteracaoId  : number;
    dataCadastro        : number;
    dataAlteracao       : number;
}