import { NgModule } from '@angular/core';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ValidationForm } from './validation.form';

@NgModule({

    providers: [
        ValidationForm
    ],


})
export class HorusFormModule { }
