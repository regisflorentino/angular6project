import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { AutenticacaoClient } from 'app/client/horus/seguranca/oauth/autenticacao.client';
import { CookieService } from 'ngx-cookie-service';
import { UsuarioClient } from '../../client/horus/seguranca/usuario/usuario.client';



@Injectable()
export class AuthenticatedGuard implements CanActivate {
    
    constructor(
        private usuarioClient: UsuarioClient,
        private authenticationClient : AutenticacaoClient, 
        private cookieService: CookieService,
        private router: Router
    ){}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
        if(state.url === '/login'){
            return true;
        }
        let accessToken = this.cookieService.get('access_token');

        if(!accessToken){
            return this.redirectToLogin();
        }

        this.refreshToken(accessToken);

        return true;
    }

    private refreshToken(accessToken) {
        this.usuarioClient.findUsuarioLogado().subscribe(
            usuarioLogado => {
                this.authenticationClient.refreshToken(usuarioLogado.login, accessToken).subscribe(
                    refreshToken => {
                        this.cookieService.delete('user_logged');
                        this.cookieService.delete('access_token');
                        this.cookieService.set('user_logged', JSON.stringify(usuarioLogado));
                        this.cookieService.set('access_token', refreshToken.access_token);
                    }
                );
            }, 
            error => {
                this.authenticationClient.logout();
                return this.redirectToLogin();
            }
        );
    }

    private redirectToLogin() {
        this.cookieService.delete('access_token');
        this.router.navigate(['/login']);
        return false;
    }
}