base.dtable = function(){
	var object = {};
	
	var requestItem = function(item){
		if (!item.data().id) {
			return;
		}
		
		var content = item.find(".base-dtable-item-content").find(".base-dtable-item-content-replace")
		
		content.find(".base-dtable-item-left").find("tbody").html("<tr><td><i class='fa fa-circle-o-notch fa-spin'></i></td></tr>");
		content.find(".base-dtable-item-right-content").find("tbody").html("<tr><td><i class='fa fa-circle-o-notch fa-spin'></i></td></tr>");
		
		var form = item.parents("form");
		var requestData = form.serializeObject();
		requestData = base.merge_objects(requestData, item.data());
		$.post(form.data("item-action"), requestData, function(response){
			content.replaceWith(response);
			
			item.parents(".base-dtable").find(".base-dtable-header-right-content").find("table").find("button[data-collapsed]").each(function(index, object){
				if ( $(object).data("collapsed") ){
					item.find("td[data-regional-id='" + $(object).data("regional-id") + "']").hide();
				}
			});
			
			item.trigger("base.requestCompleted");
			form.find(".base-dtable-scroll-x").trigger("scroll");
			
			var dTable = $(".base-dtable");
			dTable.find(".base-dtable-content").find(".base-dtable-item-right-content").scroll(function() {
				var scrollX = $(".base-dtable-scroll-x");
				scrollX.scrollLeft($(this).scrollLeft());
				dTable.find(".base-dtable-content").find(".base-dtable-item-right-content").scrollLeft($(this).scrollLeft());
				dTable.find(".base-dtable-header-right-content").scrollLeft($(this).scrollLeft());
			});
			
			dTable.find("input[type='text']").focusin(function(){
				var element = $(this);
				
				var table = element.parents("table");
				var container = table.parents(".base-dtable-item-right-content");
				var scrollBar = container.parents(".base-dtable").find(".base-dtable-scroll-x");
				var scrollWindow = scrollBar.scrollLeft() + container.width() > table.width() ? scrollBar.scrollLeft() + table.width() - (scrollBar.scrollLeft() + container.width()) : scrollBar.scrollLeft() + container.width();
				
				var elementPosition = scrollBar.scrollLeft() + element.position().left + element.width();
				
				if ( elementPosition > scrollWindow ){
					scrollBar.scrollLeft(scrollBar.scrollLeft() + element.position().left);
				}else if ( element.position().left < 0 ){
					scrollBar.scrollLeft(elementPosition < scrollBar.width() ? 0 : scrollBar.scrollLeft() + element.position().left);
				}
			});
		});
	}
	
	var resize = function(element){
		var height = element.find(".base-dtable-topbar").height() + element.find(".base-dtable-header").height();
		element.find(".base-dtable-content").css({top:height});
	}
	
	object.initElement = function(element){
		resize(element);
		
		var headerRight = element.find(".base-dtable-header-right-content");
		var width = headerRight.find("table").width() + 20;
		var dTable = $(".base-dtable");
		var scrollX = element.find(".base-dtable-scroll-x");
		
		scrollX.find(".base-dtable-scroll-x-content").css({width:width});
		scrollX.scroll(function(e){
			dTable.find(".base-dtable-header-right-content").scrollLeft($(this).scrollLeft());
			dTable.find(".base-dtable-content").find(".base-dtable-item-right-content").scrollLeft($(this).scrollLeft());
		});
		
		headerRight.find("button[data-collapsed]").click(function(e){
			var button = $(this);
			var regionalId = button.data("regional-id");
			var collapsed = button.data("collapsed");
			
			var ths = $(this).parents("thead").find("th[data-regional-id='" + regionalId + "']");
			
			if ( collapsed ){
				button.parents(".base-dtable").find(".base-dtable-content").find("td[data-regional-id='" + regionalId + "']").show();
				ths.show();
				button.parent("th").attr("colspan", ths.length + 1);
				button.find("i").removeClass("fa-folder").addClass("fa-folder-open");
			}else{
				button.parents(".base-dtable").find(".base-dtable-content").find("td[data-regional-id='" + regionalId + "']").hide();
				ths.hide();
				button.parent("th").attr("colspan", 1);
				button.find("i").removeClass("fa-folder-open").addClass("fa-folder");
			}

			button.data("collapsed", !collapsed);
			
			var width = button.parents(".base-dtable").find(".base-dtable-header-right-content").find("table").width() + 20;
			button.parents(".base-dtable").find(".base-dtable-scroll-x-content").css({width:width});
		});
		
		var items = element.find(".base-dtable-item");
		items.find(".base-dtable-item-header").find("button").click(function(e){
			var button = $(this);
			var collapsed = button.data("collapsed");
			var item = button.parents(".base-dtable-item");
			
			if ( collapsed ){
				button.parents(".base-dtable-item").find(".base-dtable-item-content").removeClass("collapsed");
				button.find("i").removeClass("fa-folder").addClass("fa-folder-open");
				
				if(!item.data("loaded")){
					item.data("loaded", true);
					
					requestItem(item);
				}else{
					item.parents(".base-dtable").find(".base-dtable-header-right-content").find("table").find("button[data-collapsed]").each(function(index, object){
						if ( $(object).data("collapsed") ){
							item.find("td[data-regional-id='" + $(object).data("regional-id") + "']").hide();
						}
					});
				}
			}else{
				button.parents(".base-dtable-item").find(".base-dtable-item-content").addClass("collapsed");
				button.find("i").removeClass("fa-folder-open").addClass("fa-folder");
			}
			
			button.data("collapsed", !collapsed);
			
			item.trigger("base.collapse");
		});
		
		items.on("base.reload", function(){
			requestItem($(this));
		});
		
		element.on("base.resize", function(){
			resize($(this));
		});
		
		element.find(".base-loading").hide();
	}
	
	object.init = function(){
		$(".base-dtable").each(function(index,element){
			object.initElement($(element));
		});
	}
	
	
	return object;
}
