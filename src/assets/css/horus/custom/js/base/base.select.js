base.select = function(){
	var object = {};
	
	object.initElement = function(element){
		element.chosen({allow_single_deselect:true}); 
		
		$(".chosen-results").click(function(e){
			$(".fa-trash-o").remove();
			$("#filiais_chosen span").append("<i class='fa fa-trash-o icon-on-right' style='position: absolute; background-color: #FAFAFA!important; top: 6px;  right: 24px; display: block;  width: 12px; height: 12px;  background: url('chosen-sprite.png') -42px 1px no-repeat;  font-size: 43px;'></i>")
		});
		
	};
	
	object.init = function(){
		$('.base-select').each(function(index, obj){
			object.initElement($(obj));
		});
	}
	
	return object;
};
