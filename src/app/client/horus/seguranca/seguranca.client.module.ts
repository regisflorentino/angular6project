import { NgModule, Injector } from '@angular/core';
import { CommonModule } from '@angular/common'
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { AutenticacaoClient } from 'app/client/horus/seguranca/oauth/autenticacao.client';
import { UsuarioClient } from './usuario/usuario.client';
import { MenuClient } from './usuario/menu.client';
import { HorusResourceModule } from 'app/client/horus/resource/horus.client.resource.module';


@NgModule({
    providers: [
        AutenticacaoClient,
        UsuarioClient,
        MenuClient
    ],
    
    imports: [
        HttpModule,
        HorusResourceModule
    ]
})
export class SegurancaClientModule {
}
