import { NgModule, Injector } from '@angular/core';
import { CommonModule } from '@angular/common'
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { SegurancaClientModule } from './seguranca/seguranca.client.module';
import { ProdutoClientModule } from 'app/client/horus/produto/produto.client.module';
import { WorkflowClientModule } from 'app/client/horus/workflow/workflow.client.module';
import { HorusResourceModule } from 'app/client/horus/resource/horus.client.resource.module';



@NgModule({
    imports: [
        CommonModule, 
        HorusResourceModule,
        SegurancaClientModule,
        ProdutoClientModule,
        WorkflowClientModule
    ],
})
export class ClientModule {
}
