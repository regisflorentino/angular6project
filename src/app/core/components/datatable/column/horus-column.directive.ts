import { Directive, Input, OnInit } from "@angular/core";

@Directive({
    selector: 'horus-column'
})
export class HorusColumn implements OnInit {

    @Input() id : string; 
    @Input() header : string;
    @Input() width : number;
    @Input() prop : string;
    @Input() filter : string;
    @Input() sort : string;
    @Input() filterMatchMode: 'EQUALS' | 'CONTAINS';
    
    ngOnInit(): void {
        if(!this.filterMatchMode){
            this.filterMatchMode = 'EQUALS'
        }
    }

}