import { Injectable } from "@angular/core";
import { ClientResource } from "app/core/client/client.resource";
import { HorusClient } from "app/client/horus/horus.client";
import { environment } from "environments/environment";

@Injectable()
@ClientResource({
    path: '/processor',
    endPoint: environment.serviceEndPoints.workflow
})
export class WorkflowProcessorClient extends HorusClient<any>{

    compareInstanceValues(expected: any, actual: any){
        return this.post({
            path: '/compareInstanceValues',
            parameters: JSON.stringify({expected: expected, actual: actual}),
            applicationType: 'application/json'
        });
    }

    findWorkflowsPrimaryTriggers(entity: any, programaCodigo: string){
        return this.post({
            path: '/findWorkflowsPrimaryTriggers',
            parameters: JSON.stringify({programName: programaCodigo, instance: entity}),
            applicationType: 'application/json'
        });
    }




}