import { Observable } from "rxjs";
import { Page } from "../../core/client/page";
import { PaginationParameter } from "app/core/components/datatable/pagination/datatable.parameter";

export interface PaginationClient<T> {

    page(parameters: PaginationParameter<T>): Observable<Page<T>>;
    
    count(entity ?: T): Observable<number>;

}