import { Component, ViewEncapsulation } from "@angular/core";
import { horusAnimation } from "../../../../../../core/animations";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { SelectItem } from "primeng/components/common/selectitem";
import { TranslateService } from "@ngx-translate/core";
import { HorusTranslationLoaderService } from "app/core/service/translation-loader.service";

import { locale as portugues } from '../i18n/pt';

@Component({
    templateUrl: './manutencao.produto.list.component.html',
    styleUrls: ['./manutencao.produto.list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: horusAnimation
})
export class ManutencaoProdutoListComponent {

    modelForm: FormGroup;
    modelFormErrors: any;
    
    constructor(
    ){
    }

    onChangeForm() {
        for (const field in this.modelFormErrors) {
            if (!this.modelFormErrors.hasOwnProperty(field)) {
                continue;
            }

            this.modelFormErrors[field] = {};
            const control = this.modelForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.modelFormErrors[field] = control.errors;
            }
        }
    }

    save(event){

    }

    createForm() {
    }

}