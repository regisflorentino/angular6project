export class EndPoint {

    constructor(private path: string, private context: string, private port:number) {}

    getEndPointPath(){
        let url = this.path;
        if(this.port){
            url=url+":" + this.port
        }
        if(this.context){
            url=url+"/"+this.context;
        }
        return url;
    }

}