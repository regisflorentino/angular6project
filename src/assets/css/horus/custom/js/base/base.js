var base = {};

base.array = function(){
	
	var object = {};
	
	object.intersectArray = function(a, b){
		var ai=0, bi=0;
		var result = new Array();

		while( ai < a.length && bi < b.length ){
			if(a[ai] < b[bi] ){
				ai++;
			}else if (a[ai] > b[bi] ){
				bi++; 
			}else{
				result.push(a[ai]);
				ai++;
				bi++;
			}
		}

		return result;
	};
	
	return object;
}

base.merge_objects = function (obj1,obj2){
	var obj3 = {};
	for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
	for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
	return obj3;
}

base.submit_new_form_in_body = function(newForm){
	$newForm = $(newForm);
	$newForm.addClass("hide");
	var newFormInBody = $newForm.appendTo("body");
	newFormInBody.submit();
}

base.init = function(){
	$("[data-toggle='tooltip']").tooltip();
	
	$.ajaxPrefilter( function(settings, localSettings, jqXHR) {
		jqXHR.done(function(data, status, xhr){
			if(data != null && $.type(data) == $.type(new String("")) && data.indexOf('data-session="valid"') >= 0){
				xhr.status = 403;
				xhr.abort();
				window.location.href = CONTEXT_ROOT;
			}
		});
		
		jqXHR.fail(function(xhr, status, error){
			if (xhr.status == 403){
				window.location.href = CONTEXT_ROOT;
			}
		});
	});
	
	$(window).scroll(function(){
		if($(this).scrollTop() + $(this).height() == $(document).height()){
			$(".scroll-content").scrollTop($(".scroll-content").height());
		}else{
			$(".scroll-content").scrollTop(-$(".scroll-content").height());
		}
	});
	
	if($("#avisoSenhaExpirar").val()){
		$.gritter.add({
			text: $("#avisoSenhaExpirar").val(),
			sticky: false,
			time: '',
			class_name: 'gritter-info gritter-light'
		});
	}
	
}