import { BehaviorSubject } from "rxjs";

export interface PaginationParameter<T> {
    
    limitPage: number;
    pageNumber: number;
    model : T;
    totalItems: number;
    sortField: string;
    sortDirection: string;

}