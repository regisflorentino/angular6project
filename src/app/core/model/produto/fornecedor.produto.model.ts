export interface FornecedorProdutoModel {

    id              : number;
    produtoId       : number;
    fornecedorId    : number;
    pessoaId        : number;
    codigo          : number;

    documento       : string;
	descricao       : string;
	uf              : string;
    cnpj            : string;
    
    principal       : boolean;
	status          : boolean;
	calcSuframa     : boolean;
	
	ativo           : boolean;
	newFornecedor   : boolean;

    tipo : 'NAO_SE_APLICA' | 'PRODUTO_BASE' | 'DERIVADO';
    tipoFornecedorExcecao : 'NAO_SE_APLICA' | 'PRODUTO_BASE' | 'DERIVADO';
    
}