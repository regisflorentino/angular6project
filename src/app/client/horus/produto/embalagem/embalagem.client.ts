import { resource } from "selenium-webdriver/http";
import { environment } from "environments/environment";
import { HorusClient } from "app/client/horus/horus.client";
import { ClientResource } from "app/core/client/client.resource";
import { Embalagem } from "app/core/model/embalagem/embalagem.model";
import { Injectable } from "@angular/core";

@Injectable()
@ClientResource({
    path: 'embalagem',
    endPoint: environment.serviceEndPoints.produto
})
export class EmbalagemClient extends HorusClient<Embalagem>{
   

    
}