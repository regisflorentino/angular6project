import { NgModule } from '@angular/core';
import { AuthenticatedGuard } from './authenticated.guard';
import { SegurancaClientModule } from '../../client/horus/seguranca/seguranca.client.module';


@NgModule({

    providers: [
        AuthenticatedGuard
    ],

    imports: [
        SegurancaClientModule,
    ]

})
export class GuardComponent { }
