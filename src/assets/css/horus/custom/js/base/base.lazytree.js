base.lazytree = function () {

    var object = {};

    function initElement(element) {
        element.jstree({
            core: {
                animation: false,
                multiple: true,
                themes: {
                    icons: false,
                    responsive: true
                },
                strings: {
                    "Loading ...": " "
                },
                data: {
                    url: element.attr('data-url'),
                    data: function (node) {
                        return {"id": node.id === "#" ? 0 : node.id};
                    }
                }
            },
            keep_selected_style: {
                cascade: false
            },
            plugins: ["changed", "checkbox"]
        });

        var div = $('<div class="lazyTreeData"></div>');
        element.parent().append(div);
        var name = element.attr("data-name");
        var updateLazyTreeData = function (e, data) {
        	$.each(data.changed.selected, function(i, obj) {
        		var input = $('<input type="hidden" />');
            	input.attr('name', name);
            	input.val(obj);
				div.append(input);	
            });
        	$.each(data.changed.deselected, function(i, obj) {
				div.find('input[value="' + obj + '"]').remove();
            });
        };
        element.on("changed.jstree", updateLazyTreeData);
    };

    object.init = function () {
        $(".lazy-tree").each(function (index, obj) {
            initElement($(obj));
        });
    };

    return object;
};