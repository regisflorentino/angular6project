import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BlankDashboardComponent } from './dashboard.blank.component';
import { PrimefacesModule } from '../../../../core/components/primefaces.module';
import { HorusTemplateModule } from 'app/main/template/template.module';
import { ComponentsModule } from '../../../../core/components/components.module';



const routes: Routes = [
    {
        path: '',
        component: BlankDashboardComponent,
    },

];

@NgModule({
    declarations: [
        BlankDashboardComponent,
    ],
    imports: [
        RouterModule.forChild(routes),
        PrimefacesModule,
        HorusTemplateModule,
        ComponentsModule
    ]
})
export class BlankDashboardModule {
}
