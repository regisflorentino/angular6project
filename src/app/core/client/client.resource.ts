import { EndPoint } from "app/core/client/endpoint.server";
import { ClientParameter } from "app/core/client/resource.parameter";
import { HorusClient } from "app/client/horus/horus.client";

export function ClientResource(annotation: ClientParameter) {
  return function decorator(target) {
    let url = annotation.path;
    let endPoint = annotation.endPoint;
    
    if(url){
      Object.defineProperty(target.prototype, 'path', { value: annotation.path });
    }

    if(endPoint){
      Object.defineProperty(target.prototype, 'endpoint', { value: annotation.endPoint });
    }

  }
};

export interface IClientResourceBuilder {
  buildResourceUrl(client: HorusClient<any>);
}