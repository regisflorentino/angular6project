import { Component, ContentChildren, QueryList, ContentChild, ViewChild, Input, ChangeDetectorRef, Output, IterableDiffers, DefaultIterableDiffer } from "@angular/core";
import { HorusColumn } from "app/core/components/datatable/column/horus-column.directive";
import { Table, SortIcon } from "primeng/table";

import { locale as portugues } from './i18n/pt';
import { HorusTranslationLoaderService } from "app/core/service/translation-loader.service";
import { DataTableClientSidePipe } from "app/core/components/datatable/filter/filter.client.side.pipe";
import { EventEmitter } from "@angular/core";
import { PaginationClient } from "app/client/horus/pagination.client";
import { Observable, BehaviorSubject } from "rxjs";
import { Page } from "../../client/page";
import { PaginationParameter } from "app/core/components/datatable/pagination/datatable.parameter";

@Component({
    selector: 'horus-dtable',
    templateUrl: './datatable.component.html',
    styleUrls: ['./datatable.component.scss']
})
export class HorusDataTableComponent<T> {

    @ViewChild('dataTableComponent') dataTableComponent: Table;

    @ContentChildren(HorusColumn, { descendants: true }) columns: QueryList<HorusColumn>;

    @Input() value: Array<T>;
    @Input() limitPage: number;
    @Input() pageNumber: number;

    @Input() dataKey: string;

    @Input() pagination: 'SERVER' | 'CLIENT';
    selected: Array<T>;
    model: any = {};

    private countItems: BehaviorSubject<number> = new BehaviorSubject<number>(null);

    private rows: Array<T>;

    private totalPages: number;

    private totalItems: number;

    private sortIcon : SortIcon;

    @Output() countMethod: EventEmitter<T> = new EventEmitter<T>();
    @Output() pageMethod : EventEmitter<PaginationParameter<T>> = new EventEmitter<PaginationParameter<T>>();

    @Output() onSelect : EventEmitter<any> = new EventEmitter<any>();
    @Output() onUnSelect : EventEmitter<any> = new EventEmitter<any>();


    constructor(
        private horusTranslationLoaderService: HorusTranslationLoaderService,
        private dataTableClientSidePipe: DataTableClientSidePipe) {
        this.horusTranslationLoaderService.loadTranslations([portugues]);
        this.limitPage = 5;
        this.pageNumber = 1;
    }

    changePageLimit(pageLimit: any) {
        this.changePage(pageLimit, 'limitPage');
    }

    changePageNumber(pageNumber: any) {
        this.changePage(pageNumber, 'pageNumber');
    }

    changePage(input: any, field: string) {
        let value = parseInt(input.target.value);
        if (Number.isNaN(value) === false && value > 0) {
            this[field] = value;
        } else {
            event.target['value'] = this[field];
        }
        this.pageNumber = 1;
        this.pageData();

    }

    firstPage(event) {
        event.preventDefault();
        this.pageNumber = 1;
        this.pageData();
    }

    previousPage(event) {
        event.preventDefault();
        if (this.pageNumber > 1) {
            this.pageNumber--;
        }
        this.pageData();
    }

    nextPage(event) {
        event.preventDefault();
        if (this.pageNumber < this.totalPages) {
            this.pageNumber++;
        }
        this.pageData();
    }

    lastPage(event) {
        event.preventDefault();
        this.pageNumber = this.totalPages;
        this.pageData();
    }

    hasOneSelected(){
        return this.selected && this.selected.length === 1;
    }

    hasSelected(){
        return this.selected && this.selected.length > 0;
    }

    pageData() {
        if (this.pagination === 'CLIENT') {
            this.clientPageSide();
        } else {
            
            this.pageMethod.emit({
                    limitPage: this.limitPage ? this.limitPage : 5,
                    pageNumber: this.pageNumber ? this.pageNumber - 1 : 0,
                    model : this.model ? this.model : {},
                    totalItems: this.totalItems ? this.totalItems : 0,
                    sortField: this.sortIcon ? this.sortIcon.field : '',
                    sortDirection: this.sortIcon ? (this.sortIcon.sortOrder ? 'ASC' : 'DESC') : ''
                }
            );
        }
    }

    private clientPageSide() {
        let valueFilter = this.value;
        if (valueFilter) {
            for (let index in this.model) {
                let columnDefinition = this.columns.filter(column => column.prop === index);
                if (columnDefinition.length === 1) {
                    valueFilter = this.dataTableClientSidePipe.transform(valueFilter, columnDefinition[0], index, this.model[index]);
                }
                if (columnDefinition.length > 1) {
                    throw new Error('Colunas para fields duplicadas, field = ' + index);
                }
            }
            this.totalPages = Math.ceil(valueFilter.length / this.limitPage);
            let start = (this.pageNumber - 1) * this.limitPage;
            let end = start + this.limitPage;
            this.rows = valueFilter.slice(start, end);
        } else {
            this.totalPages = 0;
        }
    }

    sortBy(event: any){
        this.sortIcon = event;
    }

    onRowSelect(event){
        console.log(this.selected);
        this.onSelect.emit({event});
    }

    onRowUnselect(event){
        console.log(this.selected);
        this.onUnSelect.emit({event});
    }

    selectAll(event){
        console.log(this.selected);
    }

    ngOnInit() {
        if(!this.dataKey){
            this.dataKey = 'id';
        }
        this.countItems.asObservable().subscribe(
            success => {
                if (success) {
                    this.pageData();
                }
            }
        )

        if (!this.pagination) {
            this.pagination = 'CLIENT';
            if (this.value) {
                this.totalItems = this.value.length;
                this.countItems.next(this.totalItems);
            }
        }

        if(this.pagination === 'SERVER'){
            this.countMethod.emit(this.model);
        }

    }


    loadServerpageData() {
        this.countItems.next(this.totalItems);
    }

    onFilter(value: string, prop: string, filterMatchMode: string) {
        this.model[prop] = value;
        this.pageNumber = 1;
        this.pageData();
    }

    public setCount(count: number){
        this.totalItems = count;
        this.loadServerpageData();
    }

    public setPagesValue(pagesValue: Page<T>){

        this.limitPage = pagesValue.numberOfElements;
        this.value = pagesValue.content;
        this.totalPages = pagesValue.totalPages;
        this.rows = this.value;

    }

}