import { NgModule } from '@angular/core';
import { HorusClientResource } from 'app/client/horus/resource/horus.client.resource';



@NgModule({
    providers: [
        HorusClientResource
    ],
})
export class HorusResourceModule {
}
