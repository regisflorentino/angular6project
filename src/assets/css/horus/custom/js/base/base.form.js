base.form = function(){
	var object = {};
	
	object.initElement = function(element){
		element.append($("<input />").prop("type", "submit").attr("style", "visibility:hidden;position:absolute;top:0;left:0"));
		
		element.find("input, select").each(function(index, input){
			var input = $(input);

			input.data("old", input.val());
			input.change(function(e){
				if ( $(this).val() != $(this).data("old")){
					$(".base-form-hide-on-change").fadeOut("fast",function(e){
						$(this).remove();
					});
				}
			});
		});
		
		element.submit(function(e){
			base.loading().show();
		});
	};
	
	object.init = function(){
		$(".base-form").each(function(index,element){
			object.initElement($(element));
		});
	}
	
	return object;
}