export interface PaginacaoHorus {
    pagina?: number;
    registrosPorPagina?: number;
    totalRegistros?: number;
    totalPaginas?: number;
    sorted?: number;
    fieldSort?: string;
    
}
