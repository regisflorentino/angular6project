import { Component, Input, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { locale as portugues } from './i18n/pt';

import { environment } from 'environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { HorusTranslationLoaderService } from '../../core/service/translation-loader.service';
import { UsuarioModel } from 'app/core/model/usuario/usuario.model';
import { UsuarioClient } from 'app/client/horus/seguranca/usuario/usuario.client';
import { ViewEncapsulation } from '@angular/compiler/src/core';


@Component({
  selector: 'horus-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  
})
export class ToolbarComponent implements OnInit {

  horusEnvironment: any = environment;
  usuario: UsuarioModel;

  constructor(
    private router: Router,
    private translateService: TranslateService,
    private horusTranslateModule: HorusTranslationLoaderService,
    private cookieService: CookieService,
    private usuarioClient: UsuarioClient
  ) {
    this.horusTranslateModule.loadTranslations([portugues]);
    this.translateService.use('pt');
  }

  ngOnInit(): void {
    this.usuario = JSON.parse(this.cookieService.get('user_logged'));
  }

}
