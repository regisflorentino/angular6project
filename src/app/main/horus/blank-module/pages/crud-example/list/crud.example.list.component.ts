import { Component, ViewEncapsulation } from "@angular/core";
import { horusAnimation } from "../../../../../../core/animations";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { SelectItem } from "primeng/components/common/selectitem";

@Component({
    templateUrl: './crud.example.list.component.html',
    styleUrls: ['./crud.example.list.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: horusAnimation
})
export class CrudExampleList {

    title: string = 'TESTE';

    cities1: SelectItem[];
    selectedCity1: any;


    inputFirst: string;
    inputSecond: string;

    modelForm: FormGroup;
    modelFormErrors: any; 

    selectedValuesCheck1: string[] = []; 

    constructor(
        private formBuilder: FormBuilder
    ){

        this.cities1 = [
            {label:'Select City', value:null},
            {label:'New York', value:{id:1, name: 'New York', code: 'NY'}},
            {label:'Rome', value:{id:2, name: 'Rome', code: 'RM'}},
            {label:'London', value:{id:3, name: 'London', code: 'LDN'}},
            {label:'Istanbul', value:{id:4, name: 'Istanbul', code: 'IST'}},
            {label:'Paris', value:{id:5, name: 'Paris', code: 'PRS'}}
        ];

        this.modelForm = this.createForm();
        this.modelForm.valueChanges.subscribe(() => {
            this.onChangeForm();
        });
    }

    onChangeForm() {
        for (const field in this.modelFormErrors) {
            if (!this.modelFormErrors.hasOwnProperty(field)) {
                continue;
            }

            this.modelFormErrors[field] = {};
            const control = this.modelForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.modelFormErrors[field] = control.errors;
            }
        }
    }

    save(event){

    }

    createForm() {
        return this.formBuilder.group({
            inputFirst: [this.inputFirst, Validators.required],
            inputSecond: [this.inputSecond, [Validators.email]],
            selectedValuesCheck1: [this.selectedValuesCheck1, Validators.required],
            cities1: [this.cities1, Validators.required],
        });
    }

}