import { TransacaoContextDomainChildComponent } from "app/core/model/context/transacao.context.domain.child.model";
import { Palete } from "app/core/model/embalagem/palete.model";
import { TipoEmbalagem } from "app/core/model/embalagem/tipo.embalagem.model";
import { CodigoDeBarra } from "app/core/model/embalagem/codigo.barra.model";

export interface Embalagem {
    
    id                  : number;
    idProduto           : number;
    largura             : number;
    altura              : number;
    pesoBruto           : number;
    pesoLiquido         : number;
    profundidade        : number;
    quantidade          : number;
    qtdUndEmbalagem     : number;
    multEquipEmbQtd     : number;
    
    
    pesavel             : boolean;
	status              : boolean;
	neo                 : boolean;
	transferencia       : boolean;
    
    
    tipoQuantidade      : string;
	chave               : string;
	descricaoCompleta   : string;
    
    transacao           : TransacaoContextDomainChildComponent;
    palete              : Palete;
    
    tipo                : TipoEmbalagem;
    codigoBarras        : Array<CodigoDeBarra>;

}