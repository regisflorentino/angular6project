import { NgModule, Injector } from '@angular/core';
import { CommonModule } from '@angular/common'
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { WorkflowProcessorClient } from 'app/client/horus/workflow/workflow.client';
import { HorusResourceModule } from 'app/client/horus/resource/horus.client.resource.module';


@NgModule({
    providers: [
        WorkflowProcessorClient,
    ],

    imports: [
        HttpModule,
        HorusResourceModule
    ]

})
export class WorkflowClientModule {
}
