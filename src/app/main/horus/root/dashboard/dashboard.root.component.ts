import { Component, ViewEncapsulation } from "@angular/core";
import { horusAnimation } from "../../../../core/animations";

@Component({
    templateUrl: './dashboard.root.component.html',
    styleUrls: ['./dashboard.root.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: horusAnimation
})
export class RootDashboardComponent {

}