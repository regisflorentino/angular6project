import { Injectable } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";

@Injectable()
export class ValidationForm{

    errors: any;
    form: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
    ){

    }

    public init(_errors: any): void {
        this.errors = _errors;
    }

    public valuesChanges(){
        this.form.valueChanges.subscribe(() => {
            this.onChangeForm();
        });
    }

    buildForm(_param: any): any {
        this.form = this.formBuilder.group(_param);
    }
    
    public onChangeForm() {
        for (const field in this.errors) {
            if (!this.errors.hasOwnProperty(field)) {
                continue;
            }

            this.errors[field] = {};
            const control = this.form.get(field);

            if (control && control.dirty && !control.valid) {
                this.errors[field] = control.errors;
            }
        }
    }
}