import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from '../../../core/components/components.module';
import { CrudExampleModule } from './pages/crud-example/crud.example.module';
import { BlankDashboardModule } from './dashboard/dashboard.blank.module';
import { HorusTemplateModule } from 'app/main/template/template.module';



@NgModule({

    imports: [
        TranslateModule,
        HorusTemplateModule,
        ComponentsModule,
        CrudExampleModule,
        HorusTemplateModule,
        BlankDashboardModule,
    ],

})

export class BlankModule {
}
