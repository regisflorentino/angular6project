import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
    selector: 'messageValidation'
})
export class MessageDirective {

    @Input() message: string;
    @Input() priority: string;
    @Input() for: string;

    constructor(public el: ElementRef) {
    }
}
