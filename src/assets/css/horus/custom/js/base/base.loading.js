base.loading = function(){
	var object = {};
	
	object.show = function(){
		$("#page_loading").show();
		$(".base-action").addClass("disabled");
	};
	
	object.hide = function(){
		$("#page_loading").hide();
		$(".base-action").removeClass("disabled");
	};
	
	return object;
};