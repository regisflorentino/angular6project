import { EndPoint } from "../../../../core/client/endpoint.server";
import { environment } from "environments/environment";
import { HorusClient } from "../../horus.client";
import { Token } from "app/core/model/oauth/token.model";
import { Injectable } from "@angular/core";
import { ClientResource } from "app/core/client/client.resource";
import { Observable } from "rxjs";
import { RequestOptions, URLSearchParams } from "@angular/http";
import { map, catchError } from 'rxjs/operators';
import { RefreshTokenResponse } from "app/core/model/oauth/refresh.toke.model";

@Injectable()
@ClientResource({
    path: '/oauth',
    endPoint: environment.serviceEndPoints.seguranca
})
export class AutenticacaoClient extends HorusClient<Token>{
    
    getToken(username: string, password: string): Observable<Token>{
        let urlSearchParams = new URLSearchParams();
        urlSearchParams.set('username', username);
        urlSearchParams.set('password', password);
        urlSearchParams.set('grant_type', 'password');

        return this.post({
            path: '/token',
            publicAcess: true,
            parameters: urlSearchParams,
        });
    }

    verifyTokenValid(token: string): Observable<boolean> {
        return this.get({
            path: '/verifyTokenValid',
            publicAcess: true,
            parameters: '?token='+token,
        });
    }


    refreshToken(username: string, access_token: string): Observable<Token>{

        let urlSearchParams = new URLSearchParams();
        urlSearchParams.set('username', username);
        urlSearchParams.set('access_token', access_token);
        urlSearchParams.set('grant_type', 'refresh_token');

        return this.post({
            path: '/token',
            publicAcess: true,
            parameters: urlSearchParams,
        });
    }

    logout(){
        return this.delete({
            path: '/revoke',
        });
    }

}
