export const locale = {
    lang: 'pt',
    data: {
        'CAMM15' : {
            'ACTION': {
                'NOVO': 'Novo Produto',
                'VOLTAR': 'Voltar'
            },
            
            'MESSAGES': {
                'ERRORS': {
                    'REQUIRED':{
                        'DESCRICAO_REDUZIDA': 'Descrição Reduzida é Obrigatória',
                        'GONDOLA': 'Descrição Gondola Inválida',
                        'DESCRICAO_BASICA': 'Descrição Básica Obrigatória',
                        'MARCA': 'Marca é obrigatória',
                        'REVENDA': 'Tipo de Finalidade é Obrigatório',
                        'ORIGEM' : 'Origem é Obrigatório',
                        'PRODUTO_BASE': 'Produto Base Obrigatório'
                    },
                    'EMAIL':{
                        'DESCRICAO_REDUZIDA': 'ROLA GRANDE'
                    }
                }
            },
            'TABS': {
                'DADOS_GERAIS': {
                    'TITLE': 'Dados Gerais',
                    'IDENTIFICACAO': 'Identificação do Produto',
                    'CARACTERISTICAS': 'Caractéristicas',
                    'FOTO': 'Foto'
                },
                'FORNECEDORES':  {
                    'TITLE': 'Fornecedores',
                    'DATA_TABLE': {
                        'HEADER': {
                            'ID': 'Id',
                            'DOCUMENTO': 'Documento'
                        }
                    }
                    
                },
                'EMBALAGENS':  {
                    'TITLE': 'Embalagens',
                    'DATA_TABLE': {
                        'HEADER': {
                            'ID': 'Id',
                            'QUANTIDADE': 'Quantidade'
                        }
                    }
                },
                'EMBALAGEM_FILIAL':  {
                    'TITLE': 'Embalagem por Filial',
                },
                'PRECO':  {
                    'TITLE': 'Preço',
                },
                'FISCAL': {
                    'TITLE':  'Fiscal',
                },
                'COMERCIAL':  {
                    'TITLE': 'Comercial',
                },
                'TABELA_NUTRICIONAL': {
                    'TITLE':  'Tabela Nutricional',
                },
                'PRODUTO_BALANCA': {
                    'TITLE':  'Produto Balança',
                },
                'ANEXO':  {
                    'TITLE': 'Anexos',
                }
                
                
            },
            'LIST': {
                
            },
            'FORM': {
                'CODIGO': 'Código',
                'DESCRICAO_REDUZIDA' : 'Cupom',
                'GONDOLA': 'Descrição Gondola',
                'STATUS': 'Status',
                'DESCRICAO_BASICA': 'Descrição Básica',
                'MARCA': 'Marca',
                'FABRICANTE': 'Fabricante',
                'REF_FABRICANTE': 'Referência Fabricante',
                'REVENDA': 'Revenda',
                'PRINCIPAL_FINALIDADE': 'Principal Finalidade',
                'ORIGEM' : 'Origem',
                'TIPO_PRODUTO': 'Tipo de Produto',
                'PRODUTO_BASE': 'Produto Base',
                'DESCRICAO_TABLOID' : 'Descrição Tablóide',
                'ESTRUTURA_MERCADOLOGICA': 'Estrutura Mercadológica',
                'TIPOS_PRODUTO': {
                    'NAO_SE_APLICA': 'Não se Aplica',
                    'PRODUTO_BASE': 'Produto Base',
                    'DERIVADO': 'Derivado'
                },
            },
            'ERROR' : {
                
            }
        }
    }
};
