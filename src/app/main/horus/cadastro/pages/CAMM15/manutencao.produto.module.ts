import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HorusTemplateModule } from 'app/main/template/template.module';
import { PrimefacesModule } from '../../../../../core/components/primefaces.module';
import { ComponentsModule } from 'app/core/components/components.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DirectiveModule } from '../../../../../core/directive/diretive.module';
import { ManutencaoProdutoListComponent } from './list/manutencao.produto.list.component';
import { ManutencaoProdutoFormComponent } from './from/manutencao.produto.form.component';
import { ManutencaoProdutoTabsModule } from 'app/main/horus/cadastro/pages/CAMM15/from/tabs/manutencao.produto.tabs.module';
import { MaterialModule } from 'app/core/components/material.module';
import { AuthenticatedGuard } from 'app/core/guard/authenticated.guard';
import { ClientModule } from 'app/client/horus/horus.client.module';
import { HorusFormPage } from 'app/main/template/typescript/form.page';


const routes: Routes = [
    {
        path: 'CAMM15',
        component: ManutencaoProdutoListComponent,
        canActivate: [AuthenticatedGuard]
    },
    {
        path: 'CAMM15/form',
        component: ManutencaoProdutoFormComponent,
        canActivate: [AuthenticatedGuard]
    },
    {
        path: 'CAMM15/form/:parameters',
        component: ManutencaoProdutoFormComponent,
        canActivate: [AuthenticatedGuard]
    },
    {
        path: 'CAMM15/viewWorkflow',
        component: ManutencaoProdutoFormComponent,
        canActivate: [AuthenticatedGuard]
    },
    {
        path: 'CAMM15/viewWorkflow/:parameters',
        component: ManutencaoProdutoFormComponent,
        canActivate: [AuthenticatedGuard]
    },
    {
        path: 'CAMM15/visualizar',
        component: ManutencaoProdutoFormComponent,
        canActivate: [AuthenticatedGuard]
    },
];

@NgModule({
    providers: [
        
    ],

    declarations: [
        ManutencaoProdutoListComponent,
        ManutencaoProdutoFormComponent,
    ],
    imports: [
        ManutencaoProdutoTabsModule,
        RouterModule.forChild(routes),
        HorusTemplateModule,
        FlexLayoutModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PrimefacesModule,
        ComponentsModule,
        DirectiveModule,
        MaterialModule,
        ClientModule
    ]
})
export class ManutencaoProdutoModule {
}
