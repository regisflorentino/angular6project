export interface ClientParameter {
    path        : string;
    publicAcess?: boolean;
    parameters? : any;
    handler     ?: any;
    map         ?: any;
    applicationType ?: string;

}