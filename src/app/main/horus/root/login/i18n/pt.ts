export const locale = {
    lang: 'pt',
    data: {
        'LOGIN' : {
            'FORM': {
                'FIELD' : {
                    'USERNAME': 'Login',
                    'PASSWORD': 'Senha',
                    'WELCOME_MESSAGE': 'Feito por nós, feito para nós'
                },
                'ACTION':{
                    'ACCESS': 'Acessar',
                },
                'MESSAGE': {
                    'ERROR': {
                        'LOGIN': 'Usuário ou senha inválido'
                    }
                }
            }
        }
    }
};
