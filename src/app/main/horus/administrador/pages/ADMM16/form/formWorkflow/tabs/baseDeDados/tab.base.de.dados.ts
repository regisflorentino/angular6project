import { Component, ViewEncapsulation, Injector } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { SelectItem } from "primeng/components/common/selectitem";
import { TranslateService } from "@ngx-translate/core";
import { HorusTranslationLoaderService } from "app/core/service/translation-loader.service";

import { locale as portugues } from '../../../../i18n/pt';
import { horusAnimation } from "app/core/animations";
import { ActivatedRoute } from "@angular/router";
import { HorusFormPage } from "app/main/template/typescript/form.page";


@Component({
    selector: 'tab-base-de-dados',
    templateUrl: './tab.base.de.dados.html',
    styleUrls: ['./tab.base.de.dados.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: horusAnimation
})
export class TabBaseDadosWorkflowFormComponent extends HorusFormPage {

    constructor(protected activatedRoute: ActivatedRoute, protected injector: Injector){
        super(injector, activatedRoute, [portugues]);
    }


}