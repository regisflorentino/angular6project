base.tree = function(){
	var object = {};
	
	object.initElement = function(element){
		var root = element.find(".base-tree-root");
		root.bonsai({
			checkboxes: true,
			createCheckboxes: true
		});
	};
	
	object.init = function(){
		$(".base-tree").each(function(index, obj) {
			object.initElement($(obj));
		});
	};
	
	return object;
};