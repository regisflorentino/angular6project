import { EndPoint } from "app/core/client/endpoint.server";

export const environment = {
  production: true,
  ambiente: '',
  serviceEndPoints : {
    seguranca: new EndPoint('localhost', 'seguranca', 8180),
    produto: new EndPoint('localhost', 'produto', 8280),
  }
};
