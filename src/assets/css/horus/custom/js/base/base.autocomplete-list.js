base.autocompleteList = function(){
	var object = {};
	
	var addItem = function(element, item){
		var name = element.find("input[data-name]").data("name");
		
		var exists = element.find("input[name='" + name + "[]'][value='" + item.id + "']").length > 0;
		if (!exists) {
			
			var cloneTr = element.find(".base-autocomplete-list-item-clone").find("tr.base-autocomplete-list-item").clone();
			cloneTr.attr("data-label", item.label).attr("data-compare-label", item.label.split(" - ")[1]);
			cloneTr.find("input[type='hidden']").attr("name", name + "[]");
			cloneTr.find("input[type='hidden']").val(item.id);
			cloneTr.find("td:first-child").text(item.label);
			cloneTr.find("button").click(function(){
				$(this).off("click");
				removeItem(element, item);
			});
			
			var tbody = element.find(".base-autocomplete-list-items").find("tbody");
			var trs = tbody.children("tr");
			for (var i = 0; i < trs.length && cloneTr != undefined; i++) {
				var tr = $(trs[i]);
				var compareLabel = item.label.split(" - ")[1];
				
				if (tr.attr("data-compare-label").localeCompare(compareLabel) > 0) {
					tr.before(cloneTr);
					cloneTr = undefined;
				}	
			}
			
			if(cloneTr) {
				tbody.append(cloneTr);
			}
			
			var span = element.find(".base-autocomplete-list-count");
			var count = parseInt(span.data("count")) + 1;
			span.data("count", count);
			span.text(count);
			
			if (count == 1){
				span.parents("button").removeClass("disabled");
				span.next("i").show();
			}
		}
	};
	
	var removeItem = function(element, item){
		var $autocomplete = element.find("input[data-name]");
		var name = $autocomplete.data("name");
		
		$("input[name='" + name + "[]'][value='" + item.id + "']").parents("tr").hide(function(e){
			$(this).remove();
			
			var span = element.find(".base-autocomplete-list-count");
			var all = span.data("all");
			var count = parseInt(span.data("count")) - 1;
			span.data("count", count);
			span.text(count == 0 && all ? span.data("all-label") : count);
			
			if (count == 0){
				span.parents("button").addClass("disabled");
				span.next("i").hide();
			}

			$autocomplete.trigger("change-removed");
		});
	};
	
	object.initElement = function(element){
		if(element.data("register")) {
			return;
		}
		element.data("register", true);
		
		var input = $(element.find(".base-autocomplete-list-input"));
		base.autocomplete().initAutocomplete(input, false);
		
		input.on( "autocompleteclose", function( event, ui ) {
			$(this).val("");
			$(this).autocomplete("search","");
			$(this).trigger("change");
		});
		
		input.on("autocompleteselect",function(event,ui){
			if (ui.item.label) {
				addItem(element, ui.item);
				
				$(this).val("");
				$(this).trigger("change");
			}
	
			return false;
		});

		input.data('ui-autocomplete')._renderItem = function(ul, item){
			
			if ( ul.children("li").length == 0 ){
				var checkbox = $("<input class='ace' type='checkbox'/>").prop("checked", exists).click(function(e) {
					e.stopPropagation();
					
					if($(this).prop("checked")){
						$(this).parents("ul").find("input.item:not(:checked)").click();
					}else{
						$(this).parents("ul").find("input.item:checked").click();
					}
				});
				
				var label = $("<label/>").addClass("lbl").append(" Marcar / Desmarcar todos");
				label.click(function() {
					$(this).prev().click();
				});
				var li = $("<li/>").append(checkbox).append(label);
				li.data("ui-autocomplete-item", {value:"Marcar / Desmarcar todos"});
				li.appendTo(ul);
			}
			
			var exists = element.find(".base-autocomplete-list-items").find("input[name='" + name + "[]'][value='" + item.id + "']").length > 0;
			var checkbox = $("<input class='ace item' type='checkbox'/>").prop("checked", exists).click(function(e) {
								e.stopPropagation();
								
								var allChecked = $(this).parents("ul").find("input.item:not(:checked)").length == 0
								$(this).parents("ul").find("input[type='checkbox']").first().prop("checked", allChecked);
								
								if ($(this).prop('checked')) {
									addItem(element, item);
								} else {
									removeItem(element, item);
								}
							});
			
			var li = $("<li/>").append(checkbox).append($("<label/>").addClass("lbl").append(" " + item.label));
			li.appendTo(ul);
			
			if($(ul).find('li').length == 32){
				$('<div class="ui-autocomplete-footer"><span>Muitos registros encontrados, refine sua pesquisa.</span><div>').appendTo(ul);
			}
			
			var allChecked = ul.find("input.item:not(:checked)").length == 0
			ul.find("input[type='checkbox']").first().prop("checked", allChecked);
			
			return li;
		};
		
		element.find(".base-autocomplete-clear").click(function(){
			$(this).parents(".base-autocomplete-list").find(".base-autocomplete-list-items").find("tbody").find("button").click();
		});
		
		element.find(".base-autocomplete-list-items").find("button").click(function(){
			$(this).off("click");
			removeItem(element, {id:$(this).data("id")});
		});
	}
	
	object.init = function(){
		$(".base-autocomplete-list").each(function(index, obj) {
			object.initElement($(obj));
		});
	}
	return object;
}