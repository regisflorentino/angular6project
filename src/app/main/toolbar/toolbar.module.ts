import { NgModule } from '@angular/core';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToolbarComponent } from './toolbar.component';
import { MaterialModule } from '../../core/components/material.module';
import { PrimefacesModule } from 'app/core/components/primefaces.module';
import { ComponentsModule } from '../../core/components/components.module';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    ToolbarComponent
  ],
  imports: [
    MaterialModule,
    PrimefacesModule,
    ComponentsModule,
    CommonModule
  ],
  exports: [
      ToolbarComponent
  ]
})
export class ToolbarModule { }
