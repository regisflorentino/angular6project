base.autocomplete = function(){
	var object = {};
	
	var setAttribute = function (field, value) {
		field.val(value);
		field.trigger("change");
	}
	
	var getIcon = function(input){
		return input.nextAll("i");
	}
	
	var setIcon = function(input, value){
		getIcon(input).attr("class", value);
	}
	
	object.initAutocomplete = function(input, value){
		var controller = input.data("controller");
		var modulo = input.data("modulo").toUpperCase();
		
		if ((modulo != undefined && modulo != '') && controller.substring(1,4) != modulo) {
			controller = "/" + modulo + controller.substring(4);
		}
		controller = input.data("server") + controller;

		var name = input.data("name");

		input.wrap($("<span style='width:100%'/>").addClass("input-icon input-icon-right"));
		input.after($("<i/>").addClass(input.attr("value") && input.attr("value").length > 0 ? "ace-icon fa fa-check green" : "ace-icon fa fa-search"));
		
		input.autocomplete({
			source: function(request, response){
				setIcon(input, "ace-icon fa fa-circle-o-notch fa-spin blue");
				
				input.css("background-color", "");
				input.css("border-color", "");
				
				var term = encodeURI(request.term);
				
				var request = {};
				request["term"] = term;
				request = base.merge_objects(request, input.data());
				request["ui-autocomplete"] = undefined;
				/*for (var attrname in input.data()) {
					request[attrname] = input.attr("data-"+attrname);
				}*/
				
				$.post(controller, request, function(data){
					setIcon(input, "ace-icon fa fa-exclamation-triangle orange")
					input.css("background-color", "#FFFFFF");
					
					if(!data.length) {
						setIcon(input, "ace-icon fa fa-times red");
						input.css("background-color", "#FCE5E5");
						input.css("border-color", "#ED6666");
					}
					
					response($.map(data, function(json){
						return {
							label: json.label,
							id: json.value
						};
					}));
				});
				
				
			}
		});
		
		input.focusin(function(e) {
			if(!getIcon($(this)).is("[class*='ace-icon fa fa-check']")) {
				$(this).autocomplete("search", $(this).val());
			}
		});
		
		input.focusout(function(e) {
			if(!getIcon($(this)).is("[class*='ace-icon fa fa-check']") && $(this).val() != "") {
				setIcon($(this),"ace-icon fa fa-times red");
				$(this).css("background-color", "#FCE5E5");
				$(this).css("border-color", "#ED6666");
			}
		});
		
		input.change(function(e) {
			if ( $(this).val() == "" ){
				setIcon($(this),"ace-icon fa fa-search");
				$(this).css("background-color", "");
				$(this).css("border-color", "");
			}
		});
	}
	
	object.initElement = function(element){
		if(element.data("register")) {
			return;
		}
		element.data("register", true);
		
		var name  = element.data("name"),
	  		value = element.data("value");
		
		element.attr("value", value ?   element.data("label") : "");
		object.initAutocomplete(element, value);
		
		var attribute = $("<input>").attr({"id": name.replace(new RegExp("\\.", "g"), "_"), "name":name, "type":"hidden"}).val(value);
		element.after(attribute);

		element.on("autocompleteselect", function(event, ui) {
			setAttribute(attribute, ui.item.id);
			setIcon(element, "ace-icon fa fa-check green");
			
			element.css("background-color", "");
			element.css("border-color", "");
			element.trigger("change");
		});
		
		element.data('ui-autocomplete')._renderItem = function(ul, item){
			
			$item = $("<li></li>")
						.data("item.autocomplete", item)
						.append(item.label)
						.appendTo(ul);
			
			if($(ul).find('li').length == 31){
				$('<div class="ui-autocomplete-footer"><span>Muitos registros encontrados, refine sua pesquisa.</span><div>').appendTo(ul);
			}

			return $item; 
		};
		
		var tagValKeyPress = element.val();
		element.keypress(function(e) {
			tagValKeyPress = element.val();
		});

		element.keyup(function(e) {
			e.preventDefault();
			
			if(e.keyCode != 13) {
				if((e.keyCode == 8 || e.keyCode == 46) && getIcon(element).is("[class*='ace-icon fa fa-check']")) {
					element.val("");
				}
				
				if($.trim(element.val()) == "") {
					setIcon(element,"ace-icon fa fa-search");
					element.css("background-color", "");
					element.css("border-color", "");
				}
				
				if((tagValKeyPress != element.val() || $.trim(element.val()) == "") && attribute.val() != "") {
					setAttribute(attribute, "");
					element.trigger("change");
				}
			}
		});
	}
	
	object.init = function(){
		$(".base-autocomplete").each(function(index, obj) {
			object.initElement($(obj));
		});
	}
	return object;
}
