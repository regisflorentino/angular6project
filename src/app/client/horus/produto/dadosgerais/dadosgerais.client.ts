import { resource } from "selenium-webdriver/http";
import { environment } from "environments/environment";
import { HorusClient } from "app/client/horus/horus.client";
import { ClientResource } from "app/core/client/client.resource";
import { Embalagem } from "app/core/model/embalagem/embalagem.model";
import { Injectable } from "@angular/core";
import { DadosGeraisModel } from "../../../../core/model/produto/dadosgerais.model";
import { Observable } from "rxjs";

@Injectable()
@ClientResource({
    path: '/dadosGerais',
    endPoint: environment.serviceEndPoints.produto
})
export class ProdutoDadosGeraisClient extends HorusClient<DadosGeraisModel>{
   
    load(id: number, loaderType: string): Observable<DadosGeraisModel> {
        return this.get({
            path: '/load',
            parameters: '?id='+id + '&loaderType=' + loaderType,
        });
    }

    loadTiposDeProduto(): Observable<Array<string>>{
        return this.get({
            path: '/loadTiposDeProduto'
        });
    }

}