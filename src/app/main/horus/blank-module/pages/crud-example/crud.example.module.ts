import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CrudExampleList } from './list/crud.example.list.component';
import { CrudExampleFormComponent } from './from/crud.example.form.component';
import { HorusTemplateModule } from 'app/main/template/template.module';
import { PrimefacesModule } from '../../../../../core/components/primefaces.module';
import { ComponentsModule } from 'app/core/components/components.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DirectiveModule } from '../../../../../core/directive/diretive.module';
import { HorusDatatableModule2 } from '../../../../../core/components/horus-datatable/horus-datatable.module';
import { ClientModule } from 'app/client/horus/horus.client.module';


const routes: Routes = [
    {
        path: 'programa',
        component: CrudExampleList,
    },
    {
        path: 'programa/form',
        component: CrudExampleFormComponent,
    },
    {
        path: 'programa/form/:parameters',
        component: CrudExampleFormComponent,
    }

];

@NgModule({
    declarations: [
        CrudExampleList,
        CrudExampleFormComponent,
    ],
    imports: [
        RouterModule.forChild(routes),
        HorusTemplateModule,
        FlexLayoutModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PrimefacesModule,
        ComponentsModule,
        DirectiveModule,
        HorusDatatableModule2,
        ClientModule
        
    ]
})
export class CrudExampleModule {
}
