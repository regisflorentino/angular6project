import { Component, ViewEncapsulation } from '@angular/core';
import { horusAnimation } from '../../../../core/animations';

@Component({
    templateUrl: './dashboard.blank.component.html',
    styleUrls: ['./dashboard.blank.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: horusAnimation
})
export class BlankDashboardComponent {

}