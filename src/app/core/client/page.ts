import { Pageable } from './pageable';
import { Sort } from './sort';

export class Page<T> {
    content: Array<T>;
    pageable: Pageable;
    last: boolean;
    totalPages: number;
    totalElements: number;
    size: number; 
    number : number; 
    sort: Sort;
    numberOfElements: number;
    first: boolean;
}