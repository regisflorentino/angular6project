base.formFile = function(){
	var object = {};
	
	object.initElement = function(element){
		element.find("input[type='file']").change(function(e){
			var wrapper = $(this).parents(".base-form-file");
			
			var fullPath = $(this).val();
			if (fullPath) {
				var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
				var filename = fullPath.substring(startIndex);
				if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
					filename = filename.substring(1);
				}
				
				wrapper.find(".base-form-file-name").find("span").html(filename);
				wrapper.find(".base-form-file-button-remove").css("display", "block");
			}
		})
		
		element.find(".base-form-file-button-change").click(function(e){
			$(this).parents(".base-form-file").find("input[type='file']").click();
		})
		
		element.find(".base-form-file-button-remove").click(function(e){
			var button = $(this);
			
			button.css("display","none");

			$file = $(this).parents('div.form-group').find("input[type='file']");
			$newFile = $file.clone(true);
			$file.remove();
			$(this).parents('div.form-group').find('.base-form-file').prepend($newFile);
			
			var span = $(this).parents(".base-form-file-container").find(".base-form-file-name").find("span");
			span.html(span.data("initial-text"));
		});
	};
	
	object.init = function(){
		$(".base-form-file").each(function(index,element){
			object.initElement($(element));
		});
	}
	
	return object;
}