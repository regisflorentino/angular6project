base.button = function(){
	var object = {};
	
	var confirm = function(button, action) {
		var confirmMessage = button.data("confirm");
		if (confirmMessage) {
			bootbox.confirm(confirmMessage, function(result) {
				if (result) {
					action(button);
				}
			}); 
		} else {
			action(button);
		}
	};
	
	var actionClick = function(button){
		if ( button.data("form") ) {
			var form = $(button.data("form"));
			form.submit();
		} else {
			createFormAndSubmit(button);
		}
	};
	
	var createFormAndSubmit = function(e) {
		var method = e.data("method") || "get";
		var form = $("<form/>").attr("action", e.data("action")).attr("method", method);
		var data = $(e.data("source")).find(":input").clone();
		form.append(data);
		base.submit_new_form_in_body(form);
	};
	
	var ajaxActionClick = function(button){
		var form = button.data("form");
		if ( form ) {
			ajaxRequestByForm(button);
		} else {
			ajaxRequest(button);
		}
	};
	
	var ajaxSuccessResponse = function(button, target, data) {
		if (target) {
			var targetElement = $(target);
			targetElement.html(data);
			initBaseComponents($(target));
			if (button.data("show-dialog")) {
				var dialog = $(button.data("show-dialog"));
				showDialog(dialog);
				dialog.find('.base-autocomplete, .base-autocomplete-list-input').each(function () {
					$(this).autocomplete( "option", "appendTo", dialog);
				});		
			}
		}
		if (button.data("hide-dialog")) {
			hideDialog($(button.data("hide-dialog")));
		}
		eval(button.data("on-success"));
	};
	
	var ajaxErrorResponse = function(button, target, data) {
		if (target) {
			var targetElement = $(target);
			targetElement.html(data.responseText);
			initBaseComponents($(target));
			if (button.data("hide-dialog")) {
				var dialog = $(button.data("hide-dialog"));
				dialog.find('.base-autocomplete, .base-autocomplete-list-input').each(function () {
					$(this).autocomplete( "option", "appendTo", dialog);
				});			
			}
		}
		eval(button.data("on-error"));
	};
	
	var initBaseComponents = function(targetElement) {
		base.button().init();
		base.autocomplete().init();
		base.autocompleteList().init();
		base.masked().init();
		initElements(targetElement.parent());
		base.action().checkActionsEnable();
	};
	
	var initElements = function(targetElement) {
		targetElement.find(".base-tree").each(function() {
			base.tree().initElement($(this));
		});
		targetElement.find(".base-number-input").each(function() {
			base.numberInput().initElement($(this));
		});
		targetElement.find(".base-datepicker").each(function() {
			base.date().initElement($(this));
		});
		targetElement.find(".base-daterangepicker").each(function() {
			base.daterange().initElement($(this));
		});
		targetElement.find(".base-table-form").each(function() {
			base.table().initElement($(this));
		});
	};
	
	var ajaxRequest = function(button) {
		var source = button.data("source");
		var requestData = undefined;
		if (source) {
			requestData = $(source).find(":input").serialize()
		}
		$.ajax({
			type: button.data("method") || "get",
			url: button.data("action"),
			data: requestData,
			beforeSend: base.loading().show,
			complete: base.loading().hide,
			success: function (data) {
				ajaxSuccessResponse(button, button.data("target"), data);
			},
			error: function (data) {
				ajaxErrorResponse(button, button.data("error"), data);
			}
	    });
	};
	
	var ajaxRequestByForm = function(button) {
		var form = $(button.data("form"));
		$.ajax({
			type: form.attr("method") || "get",
			url: form.attr("action"),
			data: form.serialize(),
			beforeSend: base.loading().show,
			complete: base.loading().hide,
			success: function (data) {
				ajaxSuccessResponse(button, form.parent(), data);
			},
			error: function (data) {
				ajaxErrorResponse(button, form.parent(), data);
			}
	    });
	};
	
	object.checkActionsEnable = function(){
		$(".base-table-form .base-button.base-button-single-selection, .base-table-form .base-button.base-button-multiple-selection").each(function () {
			checkActions($(this).closest(".base-table-form"));
		});
	} 
	
	var checkActions = function(table) {
		var checkboxes = $(table).find(".base-table-check-line input:checked");
		table.find(".base-button.base-button-single-selection").hide();
		table.find(".base-button.base-button-multiple-selection").hide();
		if ( checkboxes.length == 1 ){
			table.find(".base-button.base-button-single-selection,.base-button.base-button-multiple-selection").show();
		}else if(checkboxes.length > 1) {
			table.find(".base-button.base-button-multiple-selection").show();
		}
	};
	
	object.initElement = function(element) {
		if (element.data("register")) {
			return;
		}
		element.data("register", true);
		if (element.is("[data-ajax]")) {
			element.click(function() {
				confirm($(this), ajaxActionClick);
			});
		} else {
			element.click(function() {
				confirm($(this), actionClick);
			});
			if (element.data("form")) {
				$(element.data("form")).submit(function () {
					var data = $(element.data("source")).find(":input").clone();
					var div = $("<div/>").hide().append(data);
					$(this).append(div);
				});
			}
		}
	};
	
	object.init = function(){
		$(".base-button").each(function(index, element) {
			object.initElement($(element));
		});
	};
	
	return object;
};