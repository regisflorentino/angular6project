import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HorusTemplateModule } from 'app/main/template/template.module';
import { ComponentsModule } from 'app/core/components/components.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ManutencaoProdutoDadosGeraisComponent } from './dadosGerais/dadosgerais.tab';
import { PrimefacesModule } from 'app/core/components/primefaces.module';
import { DirectiveModule } from 'app/core/directive/diretive.module';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '../../../../../../../core/components/material.module';
import { HorusFormPage } from '../../../../../../template/typescript/form.page';
import { ManutencaoProdutoFornecedoresComponent } from './fornecedores/fornecedores.tab';
import { ManutencaoProdutoEmbalagensComponent } from 'app/main/horus/cadastro/pages/CAMM15/from/tabs/embalagens/embalagens.tab';



@NgModule({
    declarations: [
        ManutencaoProdutoDadosGeraisComponent,
        ManutencaoProdutoFornecedoresComponent,
        ManutencaoProdutoEmbalagensComponent
    ],
    imports: [
        HorusTemplateModule,
        FlexLayoutModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PrimefacesModule,
        ComponentsModule,
        DirectiveModule,
        TranslateModule,
        MaterialModule

    ],
    exports: [
        ManutencaoProdutoDadosGeraisComponent,
        ManutencaoProdutoFornecedoresComponent,
        ManutencaoProdutoEmbalagensComponent
    ]

})
export class ManutencaoProdutoTabsModule {
}
