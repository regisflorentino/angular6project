base.dashboard = function(){
	var object = {};
	
	var resizePortlet = function(obj){
		$(obj).height($(obj).contents().outerHeight());
	};
	
	object.init = function(){
		$(window).ready(function(){
			$(".frame-portlet").on("load", function(){
				resizePortlet(this);
			}); 
			
			$(window).on('resize', function(){
				$(".frame-portlet").each(function(){
					resizePortlet(this);
				});
			});
		})
	};
	
	return object;
};
