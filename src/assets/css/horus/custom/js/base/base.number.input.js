base.numberInput = function(){
	var object = {};
	
	object.initElement = function(element){
		var type = element.attr("data-type");
		function getLimit(min, max, value) {
			if(value < min || value > max ){
				$(".fa-times").remove();
				element.parent().addClass("input-icon").addClass("input-icon-right"); 
				element.after("<i class='ace-icon fa fa-times red'></i>"); 
				element.css({"border-color":"rgb(237, 102, 102)","background-color":"rgb(252, 229, 229)"}); 
				element.parent().find("input[type='hidden']").val(value);
				element.parent().find("input[type='hidden']").trigger("change");

			}else{
				$(".fa-times").remove();
				element.parent().addClass("input-icon").addClass("input-icon-right"); 
				element.css({"border-color":"#d5d5d5","background-color":"rgb(255, 255, 255)"});
				element.parent().find("input[type='hidden']").val(value);
				element.parent().find("input[type='hidden']").trigger("change");
			}			
			if(element.val() == ""){
				$(".fa-times").remove();
			}
		}		
		switch(type){
		case "decimal":
				element.maskMoney({precision: parseInt(element.attr("data-precision")), allowNegative: false, thousands: '.', decimal:',', defaultZero:''});
				element.on('change',function(){
					getLimit(parseInt($(this).attr("data-min")), parseInt($(this).attr("data-max")), parseFloat($(this).val().replace(".","").replace(",",".")));
				})
				break;
		case "money":
				element.maskMoney({showSymbol: true, precision: parseInt(element.attr("data-precision")), symbol: 'R$' , allowNegative: false, thousands: '.', defaultZero:'', decimal:','});
				element.on('change',function(){
					getLimit(parseFloat($(this).attr("data-min")), parseFloat($(this).attr("data-max")), parseFloat($(this).val().replace(".","").replace(",",".").replace("R$","")));
				})
				break;
		default:
				element.maskMoney({precision: 0, allowNegative: false, thousands: '.', defaultZero:''});
				element.on('change',function(){
					getLimit(parseInt($(this).attr("data-min")), parseInt($(this).attr("data-max")), parseFloat($(this).val().replace(".","").replace(",",".")));
				})
				break; 
		}
	};
	
	object.init = function(){
		$(".base-number-input").each(function(index, obj) {
			object.initElement($(obj));
		});
	};
	
	return object;
};