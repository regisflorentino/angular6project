import { Component, AfterContentChecked, ElementRef, Input, OnInit, ViewChildren, ContentChildren } from '@angular/core';
import { horusAnimation } from '../../../animations';
import { FormGroup } from '@angular/forms';
import { QueryList } from '@angular/core/src/render3';
import { MessageDirective } from 'app/core/directive/message/message.directive';
import { NgForm } from '@angular/forms';

declare var $;

@Component({
  selector: 'h-field',
  templateUrl: './horizontal.field.component.html',
  styleUrls: ['./horizontal.field.component.scss'],
  animations: horusAnimation
})
export class HorizontalFormComponent implements AfterContentChecked {

  @Input() label: string;
  @Input() columnSizeLabel: string;
  @Input() columnSizeValue: string;
  @Input() control: FormGroup;
  @ContentChildren(MessageDirective) messages;

  errorMessage: string;

  controlName: string;

  modelFormErrors: any;

  idValue: string;

  required: boolean;
  hasError: boolean;
  constructor(private elementRef: ElementRef) {
  }

  ngOnInit() {
    this.control.valueChanges.subscribe((value) => {
      this.hasError = false;
      const fieldValidation = this.control.get(this.controlName);
      if (fieldValidation && fieldValidation.dirty && !fieldValidation.valid) {
        this.hasError = true;
      }

      let content = $(this.elementRef.nativeElement).find('.toolTip');
      for (let index of content.children()) {
        if (index.id) {
          if (this.hasError) {
            this.buildErrorMessage(fieldValidation.errors);
            $(index).addClass('hfield-form-error-value');
          } else {
            this.errorMessage = null;
            $(index).removeClass('hfield-form-error-value');
          }
        }
      }
    });

  }

  buildErrorMessage(errors) {
    for (let index of this.messages.toArray()) {
      let value = errors[index.for];
      if (value) {
        this.errorMessage = index.message
      }
    }
  }

  ngAfterContentChecked(): void {
    let content = $(this.elementRef.nativeElement).find('.toolTip');
    for (let index of content.children()) {
      let value = $(index)[0];
      $(index).addClass('wid100');
      if (!this.controlName) {
        this.controlName = $(index).attr('formcontrolname');
      }

      if (value) {
        if (!this.required) {
          this.required = value.required;
        }
        if (value.id && value.id.length === 0) {
          this.label += 'ERROR-ID-NULL';
        } else {
          this.idValue = $(index)[0].id;
        }
      }


    }
  }

}