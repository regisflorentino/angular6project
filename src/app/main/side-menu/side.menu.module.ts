import { NgModule } from '@angular/core';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MenuComponent } from './side.menu.component';
import { PrimefacesModule } from '../../core/components/primefaces.module';

@NgModule({
  declarations: [
    MenuComponent
  ],
  imports: [
    PrimefacesModule
  ],
  exports: [
    MenuComponent    
  ]
})
export class MenuModule { }
