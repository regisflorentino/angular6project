import { Component, ViewEncapsulation, ViewChild, OnInit } from '@angular/core';
import { horusAnimation } from '../../../../../../core/animations';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { SelectItem } from 'primeng/components/common/selectitem';
import { EstruturaMercadologica } from './domain/estruturaMercadologica';
import { HttpClient } from '@angular/common/http';
import { TreeNode, SortEvent, } from 'primeng/api';
import { PaginacaoHorus } from '../../../../../../core/components/horus-datatable/domain/paginacaoHorus';
import { Produto } from './domain/produto';
import { Tree } from 'primeng/tree';



@Component({
    templateUrl: './crud.example.form.component.html',
    styleUrls: ['./crud.example.form.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: horusAnimation
})

export class CrudExampleFormComponent implements OnInit {
    ngOnInit(): void {

    
    }

   @ViewChild('expandingTree')
   expandingTree: Tree;

    title:string = 'TESTE';

    url: string;

    cities1: SelectItem[];
    selectedCity1: any;
    selectedCars1: any;

    // autocomplete
    estrutura: EstruturaMercadologica;
    estruturas: EstruturaMercadologica[];
    resultados: EstruturaMercadologica[];

    paginacao: PaginacaoHorus = { pagina: 1, registrosPorPagina: 5, totalPaginas: 1, totalRegistros: 0 };

    // Tree
    resultadosTree: TreeNode[];
    resultadosTreeSelecionados: TreeNode[];

    // treetable
    filiais: TreeNode[];
    cols: any[];

    //table
    listaProdutos: Produto[] = [];
    listaProdutosSel: Produto[] = [];
    colsProduto: any[] = [];

    // form
    inputFirst: string;
    inputFirst2: string;
    inputSecond: string;

    modelForm: FormGroup;
    modelForm2: FormGroup;
    modelFormTable: FormGroup;

    modelFormErrors: any;
    modelForm2Errors: any;
    modelFormTableErrors: any;

    selectedValuesCheck1: string[] = [];

    constructor(
        private formBuilder: FormBuilder,
        private formBuilder2: FormBuilder,
        private formBuilderTable: FormBuilder,
        private http: HttpClient,
        
    ) {
      
        this.selectedCars1 = '';
      
      this.url = 'http://localhost:8000/table';
      this.cols = [
          { field: 'regional', header: 'Regional' },
          { field: 'porte', header: 'Porte' },
          { field: 'filial', header: 'Filial' },
          { field: 'texto', header: 'Texto' },
          { field: '', header: '9.0 AM' },
          
        ];
        
        this.colsProduto = [
            { field: 'codigo', header: 'Código' , tamanho: 5},
            { field: 'descricao', header: 'Descrição' , tamanho: 15},
            { field: 'finalidade', header: 'Finalidade' , tamanho: 10},
            { field: 'codigoBarra', header: 'Código de Barras' , tamanho: 15 },
            { field: 'dataInclusao', header: 'Data Inclusão', tamanho: 15 },
            { field: 'dataAlteracao', header: 'Data Alteração', tamanho: 15 },
            { field: 'usuarioInclusao', header: 'Usuário Inclusão', tamanho: 15 },
            { field: 'usuarioAlteracao', header: 'Usuário Alteração', tamanho: 15 },
            { field: 'status', header: 'Status', tamanho: 15 }
        ];
        
        // popula dados tree
        this.getTree().then(dados => this.resultadosTree = dados);
        
        // popula dados tree
        this.getTable().then(dados => {
            this.listaProdutos = dados; 
            this.paginacao.totalRegistros = dados.length;
            this.paginacao.totalPaginas = Math.ceil(dados.length / this.paginacao.registrosPorPagina); 
        });

        // busca dados treetable
        this.getTreeTable().then(dados => this.filiais = dados);

        this.cities1 = [
            {label: 'Select City', value: null},
            {label: 'New York', value: {id: 1, name: 'New York', code: 'NY'}},
            {label: 'Rome', value: {id: 2, name: 'Rome', code: 'RM'}},
            {label: 'London', value: { id: 3, name: 'London', code: 'LDN'}},
            {label: 'Istanbul', value: { id: 4, name: 'Istanbul', code: 'IST'}},
            {label: 'Paris', value: { id: 5, name: 'Paris', code: 'PRS'}}
        ];

        this.modelForm = this.createForm();
        this.modelForm.valueChanges.subscribe(() => {
            this.onChangeForm();
        });

        this.modelForm2 = this.createForm2();
        this.modelForm2.valueChanges.subscribe(() => {
            this.onChangeForm();
        });

        this.modelFormTable = this.createFormTable();


        
    }

    initTableEmbalagem(event){

    }

    pageEmbalagem(event){
        console.log(event);

    }

    // busca dados tree
    getTable() {

      return this.http.get<any>('http://localhost:8000/table.1.json')
                  .toPromise()
                  .then(data => < any[] > data);

    }

    createFormTable() {
        return this.formBuilderTable.group({
            pageSize: [this.paginacao.registrosPorPagina, Validators.required],
            title : [this.title, Validators.nullValidator]
        });
    }


    // busca dados tree
    getTree() {
        return this.http.get<any>('http://localhost:8000/tree.json')
                    .toPromise()
                    .then(data => < TreeNode[] > data);
    }

    // busca dados tree table
    getTreeTable() {
        return this.http.get<any>('http://localhost:8000/treeTable.json')
                    .toPromise()
                    .then(data => <TreeNode[]> data);
    }

    // busca dados auto complete
    getEstruturas() {
        return this.http.get<EstruturaMercadologica[]>('http://localhost:8000/estruturaMercadologica.json')
                    .toPromise()
                    .then(data => data);
    }

    
    onChangeForm() {
        for (const field in this.modelFormErrors) {
            if (!this.modelFormErrors.hasOwnProperty(field)) {
                continue;
            }        const filtered: EstruturaMercadologica[] = [];


            this.modelFormErrors[field] = {};
            const control = this.modelForm.get(field);

            if (control && control.dirty && !control.valid) {
                this.modelFormErrors[field] = control.errors;
            }
        }
    }

    onChangeForm2() {
        for (const field in this.modelForm2Errors) {
            if (!this.modelForm2Errors.hasOwnProperty(field)) {
                continue;
            }

            this.modelForm2Errors[field] = {};
            const control = this.modelForm2.get(field);

            if (control && control.dirty && !control.valid) {
                this.modelForm2Errors[field] = control.errors;
            }
        }
    }

    createForm() {
        return this.formBuilder.group({
            inputFirst: [this.inputFirst, Validators.required],
            inputSecond: [this.inputSecond, [Validators.email]],
            selectedValuesCheck1: [this.selectedValuesCheck1, Validators.required],
            cities1: [this.cities1, Validators.required],

        });
    }

    createForm2() {
        return this.formBuilder2.group({
            inputFirst2: [this.inputFirst2, Validators.required],
        });
    }

    // auto complete
    search(event) {
        const query = event.query;
        this.getEstruturas().then(data => {
            this.resultados = this.filterSearch(query, data );
        });
    }

    filterSearch(query, estruturas: EstruturaMercadologica[]): EstruturaMercadologica[] {
        const filtered: EstruturaMercadologica[] = [];
        for ( let i = 0; i < estruturas.length; i++) {
            const resultado = estruturas[i];
            if (resultado.descricao.toLowerCase().indexOf(query.toLowerCase()) >= 0) {
                filtered.push(resultado);
            }
        }
        return filtered;
    }

}
