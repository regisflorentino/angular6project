import { NgModule } from '@angular/core';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { BreadCrumbComponent } from '../core/components/breadcrumb/breadcrumb.component';
import { MainComponent } from './main.component';
import { MenuModule } from './side-menu/side.menu.module';
import { ToolbarModule } from './toolbar/toolbar.module';
import { ComponentsModule } from '../core/components/components.module';
import { MaterialModule } from '../core/components/material.module';

@NgModule({
    declarations: [
        MainComponent
    ],
    imports: [
        RouterModule,
        MenuModule,
        ToolbarModule,
        ComponentsModule,
        MaterialModule,
    ],
    exports: [
        MainComponent
    ]
})
export class MainModule { }
