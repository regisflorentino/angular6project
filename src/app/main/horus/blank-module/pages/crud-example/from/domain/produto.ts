export interface Produto {
    codigo?: number,
    descricao?: string,
    finalidade?: string,
    codigoBarra?: number,
    dataInclusao?: string,
    dataAlteracao?: string,
    usuarioInclusao?: string,
    usuarioAlteracao?: string,
    status?: string


}