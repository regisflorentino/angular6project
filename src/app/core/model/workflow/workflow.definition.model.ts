export interface WorkflowConfiguration {

    clazz: string;
    rules: Array<string>;
    fields: Array<WorkflowDependencyField>;
    dependency: Array<WorkflowDependency>

}

export interface WorkflowDependencyField {

    clazz: string;
    field: string;
    column: string;

}

export interface WorkflowDependency {

    clazz: string;
    table: string;
    reference: string;
    fields: Array<WorkflowDependencyField>;

}