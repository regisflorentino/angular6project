import { NgModule, Injector } from '@angular/core';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';
import { MainModule } from './main/main.module';
import { ComponentsModule } from './core/components/components.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { DirectiveModule } from 'app/core/directive/diretive.module';
import { AuthenticatedGuard } from 'app/core/guard/authenticated.guard';
import { GuardComponent } from 'app/core/guard/guard.moule';
import { ClientModule } from 'app/client/horus/horus.client.module';
import { FormsModule, FormBuilder } from '@angular/forms';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { FakeDbService } from 'test/mock-db/mock.db.service';

const appRoutes: Routes = [


  {
    path: 'ADM',
    loadChildren: './main/horus/administrador/administrador.module#AdministradorModule',
    canActivate: [AuthenticatedGuard]
  },


  {
    path: 'CAM',
    loadChildren: './main/horus/cadastro/cadastro.module#CadastroModule',
    canActivate: [AuthenticatedGuard]
  },


  {
    path: 'modulo',
    loadChildren: './main/horus/blank-module/blank.module#BlankModule',

  },

  {
    path: '',
    loadChildren: './main/horus/root/root.module#RootModule',
    canActivate: [AuthenticatedGuard]
  },


  {
    path: '**',
    redirectTo: '',
    canActivate: [AuthenticatedGuard]
  }
];

@NgModule({
  declarations: [
    AppComponent,

  ],

  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    TranslateModule.forRoot(),
    InMemoryWebApiModule.forRoot(FakeDbService, {
      delay: 0,
      passThruUnknownUrl: true
    }),
    ComponentsModule,
    DirectiveModule,
    MainModule,
    GuardComponent,

    FormsModule

  ],
  providers: [
    FormBuilder
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
