import { Component, ViewEncapsulation, OnInit, Input, Injector } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { HorusTranslationLoaderService, Locale } from "app/core/service/translation-loader.service";

import { locale as portugues } from '../../../i18n/pt';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { horusAnimation } from "app/core/animations";
import { DadosGeraisModel } from "app/core/model/produto/dadosgerais.model";
import { Fabricante } from "app/core/model/fabricante/fabricante.model";
import { ProdutoDadosGeraisClient } from "app/client/horus/produto/dadosgerais/dadosgerais.client";
import { ActivatedRoute } from "@angular/router";
import { HorusFormPage } from "../../../../../../../template/typescript/form.page";
import { SelectItem } from "primeng/api";
import { ValidationForm } from "app/core/forms/validation.form";

@Component({
    selector: 'manutencao-produto-dados-gerais',
    templateUrl: './dadosgerais.tab.html',
    styleUrls: ['./dadosgerais.tab.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: horusAnimation
})
export class ManutencaoProdutoDadosGeraisComponent extends HorusFormPage implements OnInit {

    codigo: number;
    produtoReferencia: any;
    
    model: DadosGeraisModel;
    derivado: boolean;

    fabricantesSearch: Fabricante [];

    tiposDeProduto: Array<SelectItem>;
    constructor(private produtoClient: ProdutoDadosGeraisClient, protected activatedRoute: ActivatedRoute, protected injector: Injector){
        super(injector, activatedRoute, [portugues]);
        this.model = new DadosGeraisModel();
        this.buildForm();
        this.tiposDeProduto = []
        this.produtoClient.loadTiposDeProduto().subscribe(
            success => {
                this.tiposDeProduto = new Array<SelectItem>();
                if(success && success.length > 0){
                    for(let index in success){
                        let value = success[index];
                        let label = 'CAMM15.FORM.TIPOS_PRODUTO.' + value;
                        this.translate.get(label).subscribe(
                            success=> {
                                this.tiposDeProduto[index] = {label: success, value : value}
                            }
                        );
                    }
                    this.model.tipoDeProduto = this.tiposDeProduto[0];
                    this.validateTipoProduto(success[0]);
                }
            }
        );        
    }

    ngOnInit(): void {
        
        if(this.id){
            this.produtoClient.load(this.id, 'FORM').subscribe(
                success=>{
                    this.model = success;
                }
            )
        }
    }
    


    private buildForm() {
        this.validationForm.init({
            statusProdutoForm: {},
            codigoProdutoForm: {},
            descricaoGondolaForm: {},
            descricaoCupomForm: {},
            descricaoBasicaForm: {},
            marcaForm: {},
            fabricanteForm: {},
            fabricanteReferenciaForm: {},
            revendaForm: {},
            principalFinalidadeForm: {},
            origemForm: {},
            tipoProdutoForm: {},
            dsTabloide: {},
            produtoBaseForm: {},
            estruturaMercadologicaSelecionada: {}
        });
        this.validationForm.buildForm({
            statusProdutoForm: [this.codigo],
            codigoProdutoForm: [{ value: this.model.codigo, disabled: true }],
            descricaoGondolaForm: [this.model.descricaoCompleta],
            descricaoCupomForm: [this.model.descricaoReduzida, [Validators.required]],
            descricaoBasicaForm: [this.model.descricaoBasica, Validators.required],
            marcaForm: [this.model.marca, Validators.required],
            fabricanteForm: [this.model.fabricante],
            fabricanteReferenciaForm: [this.model.dsReferenciaFabricante],
            revendaForm: [this.model.revenda, [Validators.required]],
            principalFinalidadeForm: [this.model.principalFinalidade],
            origemForm: [this.model.origem, [Validators.required]],
            tipoProdutoForm: [this.model.tipoDeProduto],
            dsTabloide: [this.model.dsTabloide],
            produtoBaseForm: [{ value: this.model.produtoBaseId, disabled: !this.derivado, required: this.derivado }],
            estruturaMercadologicaSelecionada: [this.model.estruturaMercadologicaSelecionada]
        });
        this.validationForm.valuesChanges();
    }

    searchFabricantes(event){
        this.fabricantesSearch = [
            {id: 1, nome: 'Teste 5', status: true},
            {id: 2, nome: 'Teste 2', status: true},
            {id: 3, nome: 'Teste 3', status: true},
            {id: 4, nome: 'Teste 4', status: true},
        ]
    }

    selectValue(option, fabricanteAuto){
        fabricanteAuto.target.value = option.nome;
    }


    changeTipoProduto(event){
        if(event && event.value){
            let tipoSelecionado = event.value.value;
            this.validateTipoProduto(tipoSelecionado);
        }
    }

    private validateTipoProduto(tipoSelecionado: any) {
        this.derivado = tipoSelecionado === 'DERIVADO';
    }

    teste(event){
        console.log('click');
    }
}