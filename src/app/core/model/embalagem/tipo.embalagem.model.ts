import { TransacaoContextDomainParentComponent } from "app/core/model/context/transacao.context.domain.parent.model";

export interface TipoEmbalagem {

	id          : number;
	descricao   : string;
	sigla       : string;
    ativo       : boolean;
    transacao   : TransacaoContextDomainParentComponent;

}