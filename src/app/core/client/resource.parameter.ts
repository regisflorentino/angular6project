import { EndPoint } from "./endpoint.server";

export interface ClientParameter {
    path ?: string;
    endPoint ?: EndPoint;
}