import { Component, ViewEncapsulation, OnInit } from "@angular/core";
import { horusAnimation } from "app/core/animations";
import { locale as portugues } from './i18n/pt';
import { HorusTranslationLoaderService } from "../../../../core/service/translation-loader.service";
import { TranslateService } from "@ngx-translate/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { ValidationForm } from "../../../../core/forms/validation.form";
import { CookieService } from "ngx-cookie-service";
import { AutenticacaoClient } from "app/client/horus/seguranca/oauth/autenticacao.client";

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: horusAnimation
})
export class LoginComponent implements OnInit{

    username: string;
    password: string;

    constructor(
        private router: Router,
        private activeRoute: ActivatedRoute,
        private translate : TranslateService,
        private fuseTranslationLoader: HorusTranslationLoaderService,
        private validationForm: ValidationForm,
        private client: AutenticacaoClient,
        private cookieService: CookieService
    ) {
        this.translate.use('pt');
        this.fuseTranslationLoader.loadTranslations([portugues]);
    }

    ngOnInit(): void {
        this.validationForm.init({
            username: {},
            password: {}
        });
        this.validationForm.buildForm(
            {
                username: [this.username, Validators.required],
                password: [this.password, Validators.required]
            }
        );
        this.validationForm.valuesChanges();
    }
    
    acessar(event){
        event.preventDefault();
        this.validationForm.errors['password'] = {};
        this.client.getToken(this.username.toUpperCase(), this.password).subscribe(
            result =>{
                let key = 'access_token';
                this.cookieService.delete(key);
                this.cookieService.set(key, result.access_token);
                this.router.navigate(['']);
            },
            error => {
                if(error.error_description === 'Bad credentials'){
                    this.validationForm.errors.password = {invalid: true};
                }
            }
        );
    }



    
}