$("[id^='horusModalNavbarActions']").each(function(){
	$(this).remove();
	$('body').append($(this));
});

$("div[data-horus-notification-read]").on("hidden.bs.modal", function() {
	var id = $(this).attr('data-horus-notification-read');

	$.post("notification/setStatus/LIDA", {'ids' : id}).done(function(data) {
		notificationUnreadUpdate();
	});
	
//	notificationCreateEventUpdate(id);
});