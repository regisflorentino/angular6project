export const locale = {
    lang: 'pt',
    data: {
        'ADMM16' : {
            'ACTION': {
                'ADD': 'Adicionar',
                'EDIT': 'Editar',
                'REMOVE': 'Remover'
            },
            
            'MESSAGES': {
                'ERRORS': {
                    'REQUIRED':{
                    },
                }
            },
            'TABS': {
                'ACIONAMENTO': {
                    'TITLE' : 'Acionamento',
                    'DATA_TABLE': {
                        'HEADER': {
                            'TABELA': 'Tabela',
                            'CAMPO': 'Campo',
                            'TIPO': 'Tipo'
                        }
                    }
                }, 
                'BASE_DE_DADOS': {
                    'TITLE' : 'Base de Dados'
                }
            },
            'LIST': {
                
            },
            'FORM': {
                'NOME': 'Workflow',
                'STATUS': 'Status',
                'MODULO': 'Modulo',
                'LISTAGEM': 'Listagem',
                'SELECIONE': 'Selecione',
                'SEGURANCA': 'Módulo de Segurança',
                'PRODUTO': 'Módulo de Produto',
                'PRODUTOSEMVENDA': 'Módulo de Produto sem Venda',
            },

            'ERROR' : {
                
            }
        }
    }
};
