import { EndPoint } from "app/core/client/endpoint.server";

export const environment = {
  production: false,
  ambiente: 'DESENVOLVIMENTO-LOCAL',
  serviceEndPoints : {
    seguranca: new EndPoint('http://localhost', null, 8180),
    produto: new EndPoint('http://localhost', null, 8280),
    produtoSemVenda: new EndPoint('http://localhost', null, 8380),
    workflow: new EndPoint('http://localhost', null, 8480),
  }
};
