import { Injectable } from "@angular/core";
import { HorusClient } from "app/client/horus/horus.client";
import { IClientResourceBuilder } from "app/core/client/client.resource";

@Injectable()
export class HorusClientResource implements IClientResourceBuilder {

    buildResourceUrl(client: HorusClient<any>){
        let path = client['path'];
        let endPoint = client['endpoint'];
        endPoint.getEndPointPath().concat('/').concat(path.replace('/', ''));
        return endPoint.getEndPointPath().concat('/').concat(path.replace('/', ''));
    }

}