import { Fabricante } from "app/core/model/fabricante/fabricante.model";

export class DadosGeraisModel {
    
	id : number;
	
	codigo : number;
	produtoBaseId : number;

	ativo : boolean;

	descricaoCompleta : string;
	descricaoReduzida : string;
	descricaoBasica : string;
	dsReferenciaFabricante : string;
	digitacaoPDV : string;
	periodoValidade : string;
	sazonalidade : string;
	pesavel : string;
	origem : string;

	principalFinalidade : any;
	tipoDeProduto : any;

	ativoImobilizado : boolean;
	fgExcecaoContrato : boolean;
	gerenciadoRub : boolean;
	insumo : boolean;
	revenda : boolean;
	usoConsumo : boolean;
	permiteMultiplicarPDV : boolean;

	dsComplemento : string;
	dsTabloide : string;

	estMerc: any;

	shelfLife : number;
	classificaoPeso : any;

	marca : any;
	fabricante : Fabricante;

	compradorCategoria : any;

	novo : boolean;

	produtoReferenciaId : number;
    transientWorkflowId : number;

	estruturaMercadologicaSelecionada: string = "ABS";
}