import { Component, ViewEncapsulation, OnInit, Input, ViewChild, ContentChild, Injector } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { HorusTranslationLoaderService, Locale } from "app/core/service/translation-loader.service";

import { locale as portugues } from '../../../i18n/pt';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { horusAnimation } from "app/core/animations";
import { DadosGeraisModel } from "app/core/model/produto/dadosgerais.model";
import { Fabricante } from "app/core/model/fabricante/fabricante.model";
import { ActivatedRoute } from "@angular/router";
import { HorusFormPage } from "../../../../../../../template/typescript/form.page";
import { SelectItem } from "primeng/api";
import { FornecedorProdutoModel } from "../../../../../../../../core/model/produto/fornecedor.produto.model";
import { PaginacaoHorus } from "app/core/components/horus-datatable/domain/paginacaoHorus";
import { ValidationForm } from "../../../../../../../../core/forms/validation.form";
import { FornecedorProdutoClient } from "app/client/horus/produto/fornecedor/fornecedor.client";
import { Embalagem } from "../../../../../../../../core/model/embalagem/embalagem.model";
import { EmbalagemClient } from "../../../../../../../../client/horus/produto/embalagem/embalagem.client";
import { HorusDataTableComponent } from "../../../../../../../../core/components/datatable/datatable.component";
import { PaginationParameter } from "app/core/components/datatable/pagination/datatable.parameter";

@Component({
    selector: 'manutencao-produto-embalagens',
    templateUrl: './embalagens.tab.html',
    styleUrls: ['./embalagens.tab.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: horusAnimation
})
export class ManutencaoProdutoEmbalagensComponent extends HorusFormPage implements OnInit {

    @ViewChild('embalagemTable') embalagemTable: HorusDataTableComponent<Embalagem>; 
    model: Embalagem;
    embalagens: Array<Embalagem>;

    constructor(private client: EmbalagemClient, protected activatedRoute: ActivatedRoute, protected injector: Injector){
        super(injector, activatedRoute, [portugues]);
    }
    
    ngOnInit(): void {
        super.ngOnInit();
        this.validationForm.init({});
        this.validationForm.buildForm({});
        this.validationForm.valuesChanges();
    }

    countEmbalagem(event: any){
        this.client.count(event).subscribe(
            success => {
                this.embalagemTable.setCount(success);
            }
        );
    }

    pageEmbalagem(event: any){
        this.client.page(event).subscribe(
            success => {
                this.embalagemTable.setPagesValue(success);
            }
        );
    }

}