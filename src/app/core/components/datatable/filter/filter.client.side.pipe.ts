import { Pipe, PipeTransform, Injectable } from '@angular/core';
import { HorusColumn } from 'app/core/components/datatable/column/horus-column.directive';

@Injectable()
export class DataTableClientSidePipe {

    transform(items: Array<any>, column: HorusColumn, fieldName: any, fieldValue: any): Array<any> {

        if(!items || !column || !fieldName || !fieldValue){
            return items;
        }

        switch(column.filterMatchMode){
            case 'EQUALS':
                return this.filterEquals(items, column, fieldName, fieldValue);
            case 'CONTAINS': 
                return this.filterContains(items, column, fieldName, fieldValue);
        }

        return items;
    }

    filterEquals(items: Array<any>, column: HorusColumn, fieldName: any, fieldValue: any): Array<any>{
        return items.filter(item => item[fieldName].toString().toUpperCase() === fieldValue.toUpperCase());
    }

    filterContains(items: Array<any>, column: HorusColumn, fieldName: any, fieldValue: any): Array<any>{
        return items.filter(item => item[fieldName].toString().toUpperCase().includes(fieldValue.toUpperCase()));
    }

}