export interface RefreshTokenResponse{
    userId          : number;
    userLogin       : string;
    token           : string;
    refreshToken    : string;
}