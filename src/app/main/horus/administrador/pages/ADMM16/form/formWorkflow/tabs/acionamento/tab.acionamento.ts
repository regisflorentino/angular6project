import { Component, ViewEncapsulation, Injector } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { SelectItem } from "primeng/components/common/selectitem";
import { TranslateService } from "@ngx-translate/core";
import { HorusTranslationLoaderService } from "app/core/service/translation-loader.service";

import { locale as portugues } from '../../../../i18n/pt';
import { horusAnimation } from "app/core/animations";
import { ActivatedRoute } from "@angular/router";
import { HorusFormPage } from "app/main/template/typescript/form.page";
import { AcionamentoWorkflowModel } from "../../../../../../../../../core/model/workflow/acionamento.model";


@Component({
    selector: 'tab-acionamento',
    templateUrl: './tab.acionamento.html',
    styleUrls: ['./tab.acionamento.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: horusAnimation
})
export class TabAcionamentoWorkflowFormComponent extends HorusFormPage {

    acionamentos: Array<AcionamentoWorkflowModel>;
    showDialog: boolean = false;

    constructor(protected activatedRoute: ActivatedRoute, protected injector: Injector){
        super(injector, activatedRoute, [portugues]);
        this.mock();
    }

    adicionar(event){
        event.preventDefault();
        this.showDialog = true;
    }

    mock(){
        this.acionamentos = new Array<AcionamentoWorkflowModel>();
        this.acionamentos.push({
            id: 1, 
            workflow: null,
            tabelaAcionamento: 'CAM_PRODUTO',
            campoTabela: 'FG_REVENDA',
            tipo:   'IN',
            referenciaTabelaAcionamento: null,
            valores: null
        })
        this.acionamentos.push({
            id: 2, 
            workflow: null,
            tabelaAcionamento: 'CAM_PRODUTO',
            campoTabela: 'FG_TESTE',
            tipo:   'IN',
            referenciaTabelaAcionamento: null,
            valores: null
        })

    }

}