import { resource } from "selenium-webdriver/http";
import { environment } from "environments/environment";
import { HorusClient } from "app/client/horus/horus.client";
import { ClientResource } from "app/core/client/client.resource";
import { Injectable } from "@angular/core";
import { Fabricante } from "app/core/model/fabricante/fabricante.model";

@Injectable()
@ClientResource({
    path: 'fabricante',
    endPoint: environment.serviceEndPoints.produto
})
export class FabricanteClient extends HorusClient<Fabricante>{
   

}