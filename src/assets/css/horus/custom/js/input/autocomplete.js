/**
 * @see horus:view-autocomplete
 */
function setAttribute(field, value) {
	field.val(value).change();
}

b.autocomplete = function(tag){
	if(tag.data("register")) {
		return;
	}
	
	tag.data("register", true);
	
	var controller		  = tag.data("controller"),
	    label             = tag.data("label"),
  		name			  = tag.data("name"),
  		value             = tag.data("value");
	
	var modulo = tag.data("modulo").toUpperCase();

	if ((modulo != undefined && modulo != '') && controller.substring(1,4) != modulo) {
		controller = "/" + modulo + controller.substring(4);
	}
	
	var attribute = $("<input>").attr({"id": name.replace(new RegExp("\\.", "g"), "_"), "name":name, "type":"hidden"}).val(value),
	    icon      = $("<i/>").attr("data-name", name).addClass(value ? "fa fa-check-circle green" : "fa fa-search").css({'margin-top':'8px', 'position':'absolute', 'margin-left':'-20px','z-index':'999' });
	tag.after(icon);
	tag.after(attribute);

	tag.attr("value", value ?  label : "");
	
	var tagValKeyPress = tag.val();
	
	tag.autocomplete({
		source: function(request, response) {				
			icon.attr("class", "fa fa-spinner");
			tag.css("background-color", "");
			tag.css("border-color", "");
			
			var indexedClassName  = $("input[data-name='" + name + "']").attr("data-indexedclassname"),
				namedQuery		  = $("input[data-name='" + name + "']").attr("data-namedquery"),
	  			paremetroAuxiliar = $("input[data-name='" + name + "']").attr("data-paremetroauxiliar");
			
			$.getJSON(controller, 
				{
					"term": 				encodeURI(request.term), 
					"namedQuery": 			namedQuery,
					"indexedClassName":		indexedClassName,
					"paremetroAuxiliar":	paremetroAuxiliar
				}
			).done(function(data) {
				icon.attr("class", "fa fa-exclamation-triangle");
				tag.css("background-color", "#FFFFFF");
				
				if(!data.length) {
					icon.attr("class", "fa fa-times red");
					tag.css("background-color", "#FCE5E5");
					tag.css("border-color", "#ED6666");
				}
				
				response($.map(data, function(json) {				
					return {
						label: json.label,
						id: json.value
					};
				}));
			});
		},
		
		select: function(event, ui) {
			setAttribute(attribute, ui.item.id);
			icon.attr("class", "fa fa-check-circle green");
			tag.css("background-color", "");
			tag.css("border-color", "");
		} 
	}).data('ui-autocomplete')._renderItem = function(ul, item){
		
		$item = $("<li></li>")
					.data("item.autocomplete", item)
					.append($("<a></a>").html(item.label))
					.append()
					.appendTo(ul);
		
		if($(ul).find('li').length == 31)
			$('<div class="ui-autocomplete-footer"><span>Muitos registros encontrados, refine sua pesquisa.</span><div>').appendTo(ul);

		return $item; 
	};

	
	tag.focusin(function(e) {			
		if(!icon.is("[class*='fa fa-check-circle']")) {
			tag.autocomplete("search", tag.val());	
		}
	});
	
	tag.focusout(function(e) {
		if(!icon.is("[class*='fa fa-check-circle']") && tag.val() != "") {
			icon.attr("class", "fa fa-times red");
			tag.css("background-color", "#FCE5E5");
			tag.css("border-color", "#ED6666");
		}			
	});

	tag.keypress(function(e) {
		tagValKeyPress = tag.val();
	});

	tag.keyup(function(e) {
		e.preventDefault();
		
		if(e.keyCode != 13) {
			if((e.keyCode == 8 || e.keyCode == 46) && icon.is("[class*='fa fa-check-circle']")) {
				tag.val("");
			}
			
			if($.trim(tag.val()) == "") {
				icon.attr("class", "fa fa-search");
				tag.css("background-color", "");
				tag.css("border-color", "");
			}
			
			if((tagValKeyPress != tag.val() || $.trim(tag.val()) == "") && attribute.val() != "") {
				setAttribute(attribute, "");
			}
		}
	});
};

$(document).ready(function(e){
	
	$(".horus-view-autocomplete").each(function(index, object) {
		b.autocomplete($(object));
	});
	
});