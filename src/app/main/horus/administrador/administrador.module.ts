import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from '../../../core/components/components.module';
import { HorusTemplateModule } from 'app/main/template/template.module';
import { RootDashboardModule } from 'app/main/horus/root/dashboard/dashboard.root.module';
import { MaterialModule } from '../../../core/components/material.module';
import { ClientModule } from '../../../client/horus/horus.client.module';
import { ManutencaoWorkflowModule } from './pages/ADMM16/manutencao.workflow.module';



@NgModule({

    imports: [
        TranslateModule,
        HorusTemplateModule,
        ComponentsModule,
        HorusTemplateModule,
        MaterialModule,
        ClientModule,
        ManutencaoWorkflowModule
    ],

})

export class AdministradorModule {
}
