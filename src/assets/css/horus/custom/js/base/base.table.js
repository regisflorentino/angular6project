base.table = function(){
	
	var object = {};
	
	var paginationFirstClick = function(){
		var input = $($(this).parent().find("input[name='paginationPosition']"));
		input.val(1);
		
		var form = $($(this).parents(".base-table-form"));
		form.submit();
	};
	
	var paginationPreviousClick = function(){
		var input = $($(this).parent().find("input[name='paginationPosition']"));
		input.val(parseInt(input.val()) - 1);
		
		var form = $($(this).parents(".base-table-form"));
		form.submit();
	};
	
	var paginationNextClick = function(){
		var input = $($(this).parent().find("input[name='paginationPosition']"));
		input.val(parseInt(input.val()) + 1);
		
		var form = $($(this).parents(".base-table-form"));
		form.submit();
	};
	
	var paginationLastClick = function(){
		var input = $($(this).parent().find("input[name='paginationPosition']"));
		var inputPaginationCount = $($(this).parent().find("input[name='paginationCount']"));
		
		input.val(inputPaginationCount.val());
		
		var form = $($(this).parents(".base-table-form"));
		form.submit();
	};
	
	var paginationSizeChange = function(){
		var form = $($(this).parents(".base-table-form"));
		form.submit();
	};

	
	var tableFilterSelectChange = function(e){
		var form = $($(this).parents(".base-table-form"));
		form.find("input[name='paginationPosition']").val(1);
		form.submit();
	};
	
	var tableGetLineId = function(){
		var url = $(this).parents("table").data('url');
		var id =  $(this).attr("data-id-masterdetail-item");

		if($(document).find(".masterDetail").length >= 1){
			$.post(url, {id: id}, function(responseData){

				if($(document).find(".masterDetail").length >= 1){
					$(document).find(".masterDetail").empty();
					$(document).find(".masterDetail").html(responseData);
					$(document).find(".wrapper").show();
					$(document).find(".closeDetail").show();
				}
			});	
		}
	};
	
	var tableFilterInputChange = function(e){
		var form = $($(this).parents(".base-table-form"));
		form.find("input[name='paginationPosition']").val(1);
		
		form.submit();
	};
	
	var tableColumnCheck = function(e){
		var checks = $(this).parents(".base-table").find(".base-table-check-line");
		var checksColumn = $(this).parents(".base-table").find(".base-table-check-column input");
		var check = checksColumn.prop('checked');
		
		checks.each(function(index, object){
			var checkbox = $(object).find("input");
			
			if (check) {
				checkbox.prop('checked','checked');
			}else{
				checkbox.removeAttr('checked');
			}
		});
		
		base.action().checkActionsEnable();
	};
	
	var tableLineCheck = function(e){
		
		var checks = $(this).parents(".base-table").find(".base-table-check-line");
		var checksColumn = $(this).parents(".base-table").find(".base-table-check-column input");
		var check = 0;		
		
		checks.each(function(index, object){
			var checkbox = $(object).find("input");			
			if (!checkbox.prop('checked')) {
				check ++;
			}
		});
		if(check == 0){
			checksColumn.prop('checked','checked');
		}else{
			checksColumn.removeAttr('checked');
		}
		
		base.action().checkActionsEnable();
	};
	
	var topbarButtonClick = function(e){
		var selected = $(this).data("selected");
		var form = $(this).parents(".base-table-form");
		var parent = $(this).parents(".base-table-top-bar-extra");
		
		parent.removeClass("btn-primary");
		
		var group = $(this).data("group") ;
		if ( group ){
			var buttons = parent.find("button[data-group='" + group + "']");
			buttons.removeClass("btn-primary");
			buttons.find("input[type='hidden']").remove();
			buttons.addClass("btn-link");
			buttons.data("selected", false);
		}
		
		if ( selected ){
			$(this).data("selected", false);
			$(this).removeClass("btn-primary");
			$(this).addClass("btn-link");
			$(this).find("input[type='hidden']").remove();
		}else{
			$(this).data("selected", true);
			$(this).removeClass("btn-link");
			$(this).addClass("btn-primary");
			$(this).append($("<input/>").attr("type", "hidden").attr("name", $(this).data("name")).val("true"));
		}
		
		form.find("input[name='paginationPosition']").val(1);
		form.submit();
	};
	
	var topbarButtonInit = function(index, object){
		var $object = $(object);
		
		if( $object.data("selected") ) {
			$object.removeClass("btn-link");
			$object.addClass("btn-primary");
			$object.append($("<input/>").attr("type", "hidden").attr("name", $object.data("name")).val("true"));
		}
	};
	
	var tableExportClick = function(e){
		var form = $(this).parents(".base-table-form");
		var loading = $(form.find(".base-loading"));
		loading.show();
		
		var formClone = form.clone();
		formClone.find("input[name='paginationPosition']").remove();
		formClone.find("input[name='paginationCount']").remove();
		
		$.fileDownload($(this).data("export-action"), {
			httpMethod:"POST",
			data: formClone.serialize(),
			successCallback:function(url){
				loading.hide();
			}
		});
	};
	
	var tableSortClick = function(e){
		var form = $(this).parents(".base-table-form");
		var field = $(this).data("field");
		var sortDirection = form.find("input[name='sortDirection']");
		var sortField = form.find("input[name='sortField']");
		
		if (field === sortField.val()){
			sortDirection.val(sortDirection.val() == 'desc' ? 'asc' : 'desc');
		}else{
			sortField.val(field);
			sortDirection.val("asc");
		}
		
		form.find("input[name='paginationPosition']").val(1);
		form.submit();
	};
	
	var itemActionRequest = function(form){
		var action = form.data("item-action");
		var rowNumbers = form.find("tr[data-id]").length;
		if ( action ){
			var requestData = form.serializeObject();
			var requestResponses = form.data("requestResponses");
			var requests = form.data("requests");
			
			form.find("tr[data-id]").each(function(index,object){
				var tr = $(object);
				var id = tr.data("id");
				
				var requestLine = !requestResponses[id] || Boolean(form.find("input[name=togglable]").val())
				
				if (requestLine){
					requestData["id"] = id;
					requests.push($.post(action, requestData, function(response){
						requestResponses[id] = response;
						tr.replaceWith(response);
						buildToggleLine(form);
						form.find("tr[data-id="+ id +"]").on("click", tableGetLineId);
					})
					
					.complete(function() {
						if(rowNumbers == form.find("tr[data-loaded]").length){
							form.find("tr[data-loaded] .toggleOption").on("click", closeAndColapseToggle);
						}
					})
					
					);
				}else{
					tr.replaceWith(requestResponses[id]);
					if(rowNumbers == form.find("tr[data-loaded]").length){
						form.find("tr[data-loaded] .toggleOption").on("click", closeAndColapseToggle);
					}
					form.find("tr[data-id="+ id +"]").on("click", tableGetLineId);
					buildToggleLine(form);
				}
			});
			
			form.data("requests", requests);
		}
	}
	
	var totalActionRequest = function(form){
		var action = form.data("total-action");
		var totalLine = form.find(".base-table-total-line");
		var requestResponses = form.data("requestResponses");
		
		if ( action && totalLine ){
			if ( !requestResponses["total"] ){
				var requestData = form.serializeObject();
				
				var requests = form.data("requests");
				requests.push($.post(action, requestData, function(response){
					requestResponses["total"] = response;
					totalLine.replaceWith(response);
				}));
				
				form.data("requests", requests);
			}else{
				totalLine.replaceWith(requestResponses["total"]);
			}
		}
	}
	
	var formSubmit = function(e){
		e.preventDefault();
		
		var form = $(this);
		
		if ( form.data("submited") != true ) {
			form.data("submited", true);
			
			var requests = form.data("requests");
			for ( var i = 0; i < requests.length; i++ ){
				var request = requests[i];
				if ( request && request.readystate != 4 ){
					request.abort();
				}
			}
			
			var loading = $(form.find(".base-loading"));
			loading.show();
			
			$.ajax({
				url:form.attr("action"),
				type:"post",
				data:form.serialize(),
				success:function(responseData){
					var responseArray = $.parseHTML(responseData, document, true);
					
					var newForm;
					for ( var i = 0; i < responseArray.length; i++ ){
						if ( $(responseArray[i]).hasClass("base-table-form") ){
							newForm = $(responseArray[i]);
						}
					}
					
					newForm.data("requestResponses", form.data("requestResponses"));
					object.initElement(newForm);
					
					newForm.find("[data-toggle='tooltip']").tooltip();
					
					form.replaceWith(newForm);
					base.action().checkActionsEnable();
					base.button().init();
				}
			});
		}
	};
	
	var closeDetail = function(){
		$(document).find(".wrapper").hide();
		$(document).find(".masterDetail").empty();
		$(this).parent().hide();
	};
	
	
	var closeAndColapseToggle = function(){
		var toggleColumn = $(this);
		var dataLoaded = $(toggleColumn).attr('id');
		toggleColumn.parent('tr').nextAll("tr[data-row="+dataLoaded+"]").toggle();

		toggleColumn.find('.glyphicon').each(function (index) {
			var iconSpan = $(toggleColumn.find('.glyphicon')[index]);
			if(iconSpan.hasClass("glyphicon-plus")){
				iconSpan.removeClass("glyphicon-plus");

				var url = toggleColumn.attr('action-toggle');
				
				if(url){
					if(!toggleColumn.hasClass('loaded')){

						iconSpan.addClass("glyphicon");
						iconSpan.addClass("fa");
						iconSpan.addClass("fa-circle-o-notch");
						iconSpan.addClass("fa-spin");

						var request;
						var table = toggleColumn.parents('form').clone();
					
						var parameterName = toggleColumn.find('.parameterName').val();
						var parameterValue = toggleColumn.find('.parameterValue').val();
						
						table.append($("<input />").attr("name", parameterName).attr("type", "hidden").val(parameterValue));
						
						var requestData = table.serializeObject();
						request = $.post(url, requestData, function(response) {
							$(toggleColumn.parent('tr').nextAll("tr[data-row="+dataLoaded+"]")).remove();
							$(response).insertAfter(toggleColumn.parent('tr').closest('tr'))
							toggleColumn.addClass('loaded');
							iconSpan.removeClass("fa");
							iconSpan.removeClass("fa-circle-o-notch");
							iconSpan.removeClass("fa-spin");
							iconSpan.addClass("glyphicon");
							iconSpan.addClass("glyphicon-minus");
						});
					} else {
						iconSpan.addClass("glyphicon");
						iconSpan.addClass("glyphicon-minus");
					}

				} else {
					toggleColumn.parent('tr').nextAll("tr[data-row="+dataLoaded+"]").removeClass('hidden');
					iconSpan.addClass("glyphicon");
					iconSpan.addClass("glyphicon-minus");
				}
				
			} else if (iconSpan.hasClass("glyphicon-minus")){
				
				if(toggleColumn.attr('load-always-open') == 'true'){
					toggleColumn.removeClass('loaded');
				}
				
				iconSpan.removeClass("glyphicon-minus");
				iconSpan.addClass("glyphicon-plus");
			}
			
		});
		

	};
	
	
	object.getSelectedIds = function(element){
		var ids = [];
		var checkBoxes = element.find(".base-table-check-line input:checked");
		checkBoxes.each(function(index,object){
			ids.push(parseInt($(object).val()));
		});
		
		return ids;
	};
	
	object.initElement = function(element){
		element.on("submit", formSubmit);
		element.find("input[name='paginationPosition']").inputmask("Regex", {regex:"[0-9]+"});
		element.find("input[name='paginationSize']").inputmask("Regex", {regex:"[0-9]+"});
		element.find(".base-table-top-bar-extra button").on("click", topbarButtonClick);
		element.find(".base-pagination-first").on("click", paginationFirstClick);
		element.find(".base-pagination-previous").on("click", paginationPreviousClick);
		element.find(".base-pagination-next").on("click", paginationNextClick);
		element.find(".base-pagination-last").on("click", paginationLastClick);
		element.find(".base-pagination-size").on("blur", paginationSizeChange);
		element.find(".filters select").on("change", tableFilterSelectChange);
		element.find(".filters input").on("change", tableFilterInputChange);
		element.find(".base-table-check-column input").on("click", tableColumnCheck);
		element.find(".base-table-check-line input").on("click", tableLineCheck);
		element.find(".base-table-top-bar-extra button").each(topbarButtonInit);
		element.find(".base-table-export-button").on("click", tableExportClick);
		element.find(".toggleOption").on("click", closeAndColapseToggle);
		
		$(document).find(".closeDetail a").on("click", closeDetail);

		var sortDirection = element.find("input[name='sortDirection']").val();
		var sortField = element.find("input[name='sortField']").val();
		element.find("th[data-field='" + sortField + "']").addClass(sortDirection);
		element.find(".base-sortable").on("click", tableSortClick);
		element.data("requests", []);
		
		if ( !element.data("requestResponses") ){
			element.data("requestResponses", {});
		}

		if ( !element.data("masterDetail") ){
			element.find(".table tbody tr").on("click", tableGetLineId);

			var line = element.find(".base-table tbody tr");
			line.each(function(index, obj){
				var idTable = $(this).find("td[data-id]").text();
				$(obj).attr({"data-id-masterdetail-item":idTable});
			});
		}

		itemActionRequest(element);
		totalActionRequest(element);
		buildToggleLine(element);
	}

	
	object.initToggle = function (element){
		element.find(".toggleOption").on("click", closeAndColapseToggle);
	}
	
	function buildToggleLine(table){
		if($(table).find('#isToggable').val() === 'true'){
			$(table).find('tbody tr').not('.toggleDetail').each(function (index, element) {
				$(element).removeClass('toggleLineEven');
				$(element).removeClass('toggleLineOdd');
				if(index % 2 == 0){
					$(element).addClass('toggleLineOdd');
				} else {
					$(element).addClass('toggleLineEven');
				}						
			});
		}
	}
	
	object.init = function(){
		
		base.action().checkActionsEnable();
		
		$(".base-table-form").each(function(index, element){
			object.initElement($(element));
		});
	};
	
	return object;
}
