import { Component, ViewEncapsulation, ContentChildren, ViewChild, OnInit, ContentChild, Injector } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { HorusTranslationLoaderService, Locale } from "app/core/service/translation-loader.service";

import { locale as portugues } from '../i18n/pt';
import { Validators } from "@angular/forms";
import { ManutencaoProdutoDadosGeraisComponent } from "app/main/horus/cadastro/pages/CAMM15/from/tabs/dadosGerais/dadosgerais.tab";
import { ActivatedRoute } from "@angular/router";
import { horusAnimation } from "app/core/animations";
import { HorusFormPage } from "app/main/template/typescript/form.page";

@Component({
    templateUrl: './manutencao.produto.form.component.html',
    styleUrls: ['./manutencao.produto.form.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: horusAnimation
})
export class ManutencaoProdutoFormComponent extends HorusFormPage implements OnInit {

    @ContentChild(ManutencaoProdutoDadosGeraisComponent) dadosGeraisTab;

    constructor(protected activatedRoute: ActivatedRoute, protected injector: Injector){
        super(injector, activatedRoute, [portugues]);
    }
    
    ngOnInit(): void {
    }

    save(event){
        event.preventDefault();
    }

}