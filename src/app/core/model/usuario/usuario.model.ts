import { TransacaoContextDomainParentComponent } from "app/core/model/context/transacao.context.domain.parent.model";

export interface UsuarioModel {

    id              : number;

	email           : string;
	login           : string;
	matricula       : string;
	nome            : string;
    senha           : string;
    
	status          : boolean;
	terceiro        : boolean;	
    regional        : boolean;	
    
	ultimoAcesso    : Date;
	ultimaSenha     : Date;
	transacao       : TransacaoContextDomainParentComponent;
	
}