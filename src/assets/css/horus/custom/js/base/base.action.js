base.action = function(){
	var object = {};
	
	var actionClick = function(e){
		
		var method = $(this).data("method");
		method = method === undefined ? "get" : method;
		
		var sendId = $(this).data("send-id") === true;
		var form = $("<form/>").attr("action", $(this).data("action")).attr("method", method);
		
		if ( sendId ){
			var checkBoxes = $(".base-table-check-line input:checked");
			checkBoxes.each(function(index,object){
				form.append($("<input/>").attr("name", "id[]").val($(object).val()));
			});
		}
		
		var id = $(this).data("id");
		
		if ( id ){
			form.append($("<input/>").attr("name", "id").val(id));
		}
		
		base.submit_new_form_in_body(form);
	};
	
	object.checkActionsEnable = function(){
		base.button().checkActionsEnable();
		
		var checkboxes = $(".base-table-check-line input:checked");
		
		$(".base-action.base-action-single-selection").hide();
		$(".base-action.base-action-multiple-selection").hide();
		
		if ( checkboxes.length == 1 ){
			var checkbox = $(checkboxes[0]);
			
			var actions = (checkbox.data("action") || "").split(",");
			for ( var i = 0; i < actions.length; i++ ){
				$("#action_" + actions[i] + ".base-action.base-action-single-selection").show();
				$("#action_" + actions[i] + ".base-action.base-action-multiple-selection").show();
			}
		}else if(checkboxes.length > 1) {
			var actions = undefined;
			checkboxes.each(function(index, object){
				var checkbox = $(object);
				
				if ( actions == undefined ){
					actions =  (checkbox.data("action") || "").split(",");
				}else{
					actions = base.array().intersectArray(actions,  (checkbox.data("action") || "").split(","));
				}
			});

			for ( var i = 0; i < actions.length; i++ ){
				$("#action_" + actions[i] + ".base-action.base-action-multiple-selection").show();
			}
		}
	};
	
	object.init = function(){
		$(".base-action[data-action]").click(actionClick);
	}
	
	return object;
};