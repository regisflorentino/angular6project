import { CookieService } from "ngx-cookie-service";

export class MockCookieService  {

    private cookies: Array<any>;

    constructor(array : any[]){
        this.cookies = new Array<string>();
        if(array){
            for(let index of array){
                this.set(index.key, index.value);
            }
        }
    }

    get(key: string){
        let current = this.findByString(key);
        return current !== null ? current.value : null;
    }

    set(key: string, value: string){
        this.cookies.push({key: key, value: value});
    }

    delete(key: string){
        let index = this.findByString(key);
        if(index){
            this.cookies.splice(parseInt(index), 1)
        }
    }

    findByString(key: string){
        console.log(this.cookies);
        for(let index of this.cookies){
            console.log(index);
            if(index.key === key){
                return index;
            }
        }
        return null;
    }


}