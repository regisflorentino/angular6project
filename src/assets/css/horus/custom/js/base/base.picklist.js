base.picklist = function() {

	var object = {};

	var onDrop = function(event, ui) {
			
		var $list = $(this);
		var $selected = $(this).parent('div').find('.selectedPickListItem');

		$helper = ui.helper;
		$($helper).removeClass("selectedPickListItem");
		
		
		$('.droppable').addClass('scrollable');
		if ($selected.length > 1) {
			funcMoveSelected($list,	$selected);
		} else {
			funcMoveItem(ui.draggable, $list);
		}
	};

	var onDrag = function(event, ui) {
		var $helper = ui.helper;
		$($helper).removeClass("selectedPickListItem");
		$($helper).addClass('moving');
		$('.droppable').removeClass('scrollable');
		var $selected = $(this).parent('div').find('.selectedPickListItem');
		$($helper).html(($selected.length > 0 ? $selected.length : 1) + " Registro(s)");
	};
	
	var onStartDrag = function(event, ui) {
		var $helper = ui.helper;
		var widthVar = $helper.width() * 0.75;
		$($helper).width(widthVar);
	};
	
	
	var funcMoveSelected = function ($list, $selected) {
		$($selected).each(function() {
			$(this).appendTo($list).removeClass("selectedPickListItem");
		});
		funcHideAfterMove($list);
	};
	
	var funcMoveItem = function moveItem($item, $list) {
		$item.find(".item").remove();
		$item.appendTo($list);
		funcHideAfterMove($list);
	};
	
	var funcHideAfterMove = function ($list){
		var side = $list.hasClass('left') ? '.left' : '.right';
		var parentDiv =  $list.parent('div');
		var elementSeach = $list.hasClass('left') ? parentDiv.find('.searchFieldLeft') : parentDiv.find('.searchFieldRight'); 
		funcHideOnSearch(elementSeach, side, elementSeach.val());	
		
		$($list).parent('div').find('.pick-list').each(function (index, element) {
			funcChangeLineColor($(element).find('.item'));
		});
		
		funcSetLabelQuantidadeRegistros(parentDiv)
		funcSetNameInputHiddenValue($(parentDiv).find('.item'));
	};
	
	var funcHideOnSearch = function (element, sideClass, valueStringFilter){
		var divParent = element.parent('div').parent('div');
		
		var $value = valueStringFilter;
		var $searchBy = divParent.find('.class_searchBy').val();
		var $searchValueItem = divParent.find('.' + $searchBy);
		
		divParent.find(sideClass).find('.item').each(function (index, element) {
			var searchElement = $(element).find('.'+$searchBy);
			if(!searchElement){
				var message = 'Um valor de classe para filtragem deve ser definido no component base:pick-list no atributo searchByClass, ou' +  
							   ' o component base:pick-list-item deve possuir o atributo label preenchido para que a filtragem funcione';
				throw new Error(message); 
			}
			
			var searchValue = searchElement.is('span') ? searchElement.text() : searchElement.val();
			
			if(searchValue.toUpperCase().indexOf($value.toUpperCase()) != -1){
				$(this).removeClass('hidden');
			} else {
				$(this).addClass('hidden');											
			}
		});	
	};
	
	var funcChangeLineColor = function ($list){
		var start = 0;
		
		$list.each(function (index, element) {
			if(!$(element).hasClass('moving') && !$(element).hasClass('hidden')){
				$(element).removeClass('oddPickListItem');
				$(element).removeClass('evenPickListItem');
				if(start % 2 != 0){
					$(element).addClass('oddPickListItem');
				} else {
					$(element).addClass('evenPickListItem');
				}	
				start ++;
				
				var icon = $(element).find('.icon-position');
				
				if(icon){
					var size = $(element).height();
					
					$(icon).width(size);
					$(icon).height(size);
				}
				
			}
		});
	};
	
	var funcSetNameInputHiddenValue = function ($list){
		$indexEnableItem = 0;
		$($list).find('.valuePickItem').each(function(index, element) {
			$(element).prop('name', '');
			
			var item = $(element).parent('div');
			
			if(!$(item).hasClass('hidden')){
				var parameter = $(element).attr('name-parameter');
				var side =  item.parent('div').find('.side');
				var name = parameter + side.val() + '[' + $indexEnableItem + ']';
				$(element).prop('name', name);
				$indexEnableItem++;
			}
		});
		funcSetLabelQuantidadeRegistros($list);
	};
	
	var funcSetLabelQuantidadeRegistros = function ($list){
		
		$($list).parent('div').find('moving').each(function (index, element){
			$(element).addClass('hidden');			
		});
		$($list).parent('div').find('.quantidadeRegistros').each(function (index, element){
			var textCount = $(element).find('.countText');
			var emptyText = $(element).find('.countEmptyText');
			var total;
			if($(element).hasClass('rightSideQtd')){
				total = funcGetSideItems($(element), '.right');
			} else if ($(element).hasClass('leftSideQtd')) {
				total = funcGetSideItems($(element), '.left');
			}
			$(element).empty();
			
			var message = total.length > 0 ? textCount.val() + ': ' + total.length : emptyText.val();
			
			$(element).append(message);
			$(element).append(textCount)
			$(element).append(emptyText)
		});
	};
	
	var funcGetSideItems = function (element, sideClass){
		return $(element).parent('div').parent('div').find(sideClass).find('.item').not('.hidden');
	};
	
	var funcMoveAllItems = function (element, sideClass){
		var pickListMainDiv = $(element).parent('div').parent('div');
		var $listSideDrop = sideClass === '.left' ? pickListMainDiv.find('.right') : pickListMainDiv.find('.left');
		var $listSideDrag = sideClass === '.left' ? pickListMainDiv.find('.left') : pickListMainDiv.find('.right');
		funcMoveSelected($listSideDrop, $listSideDrag.find('.item'));
	};
	
	var funcToggleSelectedClass = function (element) {
		if(element.find('.item_isDragAndDrop').val() == 'true'){
			element.toggleClass("selectedPickListItemDragAndDrop");
		}
		element.toggleClass("selectedPickListItem");
	};
	
	
	var funcDisableAnotherSide = function (sideClass) {
		sideClass.find('.selectedPickListItem').each(function() {
			funcToggleSelectedClass($(this));
		});
		sideClass.find('.multiple').val(0)
	};
	
	var onMoveAll = function(element) {
		funcMoveAllItems(element.target, '.left');
	};
	
	var onRemoveAll = function(element) {
		funcMoveAllItems(element.target, '.right');
	};
	
	var onKeyUpSearchLeft = function (element) {
		funcKeyUpSearch(element, '.left');
	};
	
	
	var onKeyUpSearchRight = function (element) {
		funcKeyUpSearch(element, '.right');
	};
	
	var funcKeyUpSearch = function (element, side){
		var target = $(element.target);
		funcHideOnSearch(target, side, element.target.value);
		target.parent('div').parent('div').find('.pick-list').each(function (index, element) {
			funcChangeLineColor($(element).find('.item'));
		});
		
		funcSetLabelQuantidadeRegistros($(target).parent('div'))
		funcSetNameInputHiddenValue($(target).parent('div').parent('div').find('.item'));
	};
	
	var selectItem = function() {
		var target = $(this);
		var dragAndDrop = target.find('.item_isDragAndDrop').val();
		var side = target.parent('div');
		var divDualList = side.parent('div');
		
		var $selected = $(this).parent('div').find('.selectedPickListItem');
		var $list = side.hasClass('left') ?  divDualList.find('.right') : divDualList.find('.left') ;
		
		var onDrop = $list.find('.selectedPickListItem').length == 0;
		
		$(this).parent('div').find('.multiple').val(0);
		
		if(dragAndDrop === 'false'){
			if($selected.length > 1){
				funcMoveSelected($list, $selected);
			} else {
				funcMoveItem(target, $list);
			}
		} else {
			if(onDrop){
				funcToggleSelectedClass(target);
			}
		}
		
		if(onDrop){
			side.hasClass('left') ? funcDisableAnotherSide($list) : funcDisableAnotherSide($list);
		}

	};
	
	var onSelectingMultiples = function (e) {
		var multiple = $(this).parent('div').find('.multiple');
		var isDragAndDrop = $(this).find('.item_isDragAndDrop').val();

		if(e.type == 'mousedown'){
			if($(this).find('.item_isDragAndDrop').val() == 'false'){
				multiple.val(1);
			} else {
				multiple.val(0);
			}
			
			if(isDragAndDrop == 'false'){
				funcToggleSelectedClass($(this));				
			}
		} else {
			if(multiple.val() == 1 && ! $(this).hasClass('selectedPickListItem')){
				funcToggleSelectedClass($(this));
			}
		}
	}
	
	var onPickListLeave = function (){
		var dragAndDrop = $(this).find('.list_isDragAndDrop').val();
		
		if(dragAndDrop == 'false'){
			var $selected = $(this).find('.selectedPickListItem');
			if($selected.length > 0){
				var $list = $(this).parent('div').find($(this).hasClass('left') ? '.right' : '.left');
				funcMoveSelected($list,	$selected);				
			}
			$(this).find('.multiple').val(0);			
		}

	}
	
	object.initElement = function(element) {
		var $indexThis = $(element);

		$indexThis.find('.pick-list').each(function(index, element) {
			funcSetNameInputHiddenValue(element);
			funcChangeLineColor($(element).find('.item'));
		});
		
		var pickListMainId = '#' + $indexThis.prop("id");
		$indexThis.find('.pick-list').addClass('scrollable');
		$indexThis.find('.pick-list').on('mouseleave', onPickListLeave);

		$indexThis.find('.droppable').droppable({drop : onDrop, tolerance : "touch"});
		$indexThis.find('.draggable').draggable({revert : "invalid", helper : "clone", cursor : "move", scroll : true, containment: pickListMainId,	drag : onDrag,	start : onStartDrag});
		$indexThis.find('.moveall').on('click', onMoveAll);
		$indexThis.find('.removeall').on('click', onRemoveAll);
		$indexThis.find('.searchFieldLeft').on('keyup', onKeyUpSearchLeft);
		$indexThis.find('.searchFieldRight').on('keyup', onKeyUpSearchRight);
		
		$indexThis.find('.item').on('mouseup', selectItem);
		$indexThis.find('.item').on('mousedown mouseover', onSelectingMultiples);
		
	};

	object.init = function() {
		$('.base-picklist').each(function(index, element) {
			object.initElement($(element));
		});

	};

	return object;
}
