base.tabs = function() {
	var object = {};

	var changeTabs = function(e) {
		var target = e.target
		$('div[content-component-id="' + $(target).attr('header-component-id') + '"]').removeClass('active');
		$('div[content-tab-name="' + $(target).attr('tab-name') + '"]').addClass('active');
	}

	object.initElement = function(element) {
		$(element).find('a[data-toggle="tab"]').on('shown.bs.tab', changeTabs);
	}

	object.init = function() {
		$(".base-tabs").each(function(index, element) {
			object.initElement($(element));
		});
	}

	return object;
}
