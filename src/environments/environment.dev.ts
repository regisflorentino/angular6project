import { EndPoint } from "app/core/client/endpoint.server";

export const environment = {
  production: false,
  ambiente: 'DESENVOLVIMENTO',
  serviceEndPoints : {
    seguranca: new EndPoint('http://localhost', 'horus/seguranca', 8080),
    produto: new EndPoint('http://localhost', 'horus/produto', 8080),
    workflow: new EndPoint('http://localhost', 'horus/workflow', 8080),
  }
};
