import { AcionamentoWorkflowModel } from "./acionamento.model";

export interface ValorAcionamentoWorkflowModel {

    id              : number;
    acionamento     : AcionamentoWorkflowModel;
    parametro       : string;
    

}