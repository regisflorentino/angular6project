import { Component, ViewEncapsulation } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { HttpClient } from '@angular/common/http';

import { MenuClient } from 'app/client/horus/seguranca/usuario/menu.client';

@Component({
  selector: 'horus-menu',
  templateUrl: './side.menu.component.html',
  styleUrls: ['./side.menu.component.scss'],
  
})
export class MenuComponent {
  
  items: MenuItem[];

  constructor(client: MenuClient){
    client.getMenu()
      .toPromise()
      .then(data => this.items = data);
  }



}
