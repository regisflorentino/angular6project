import { resource } from "selenium-webdriver/http";
import { environment } from "environments/environment";
import { HorusClient } from "app/client/horus/horus.client";
import { ClientResource } from "app/core/client/client.resource";
import { Embalagem } from "app/core/model/embalagem/embalagem.model";
import { Injectable } from "@angular/core";
import { DadosGeraisModel } from "../../../../core/model/produto/dadosgerais.model";
import { Observable } from "rxjs";
import { FornecedorProdutoModel } from "../../../../core/model/produto/fornecedor.produto.model";

@Injectable()
@ClientResource({
    path: '/fornecedorProduto',
    endPoint: environment.serviceEndPoints.produto
})
export class FornecedorProdutoClient extends HorusClient<FornecedorProdutoModel>{
   
    load(id: number, loaderType: string): Observable<Array<FornecedorProdutoModel>> {
        return this.get({
            path: '/load',
            parameters: '?id='+id + '&loaderType=' + loaderType,
        });
    }

}