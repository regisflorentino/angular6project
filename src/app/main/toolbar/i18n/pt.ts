export const locale = {
    lang: 'pt',
    data: {
        'TOOLBAR' : {
            'ACTION': {
                'CONECTADO_COMO': 'Conectado como',
                'ALTERAR_SENHA': 'Alterar Senha',
                'MEU_PERFIL': 'Meu Perfil',
                'SAIR': 'Sair',
                'VERSAO': 'Versão'
            },
            'LABELS': {
                'MENSAGENS': 'Mensagens',
                'VER_TODAS': 'Ver Todas'
            }
        }
    }
};
