import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from '../../../core/components/components.module';
import { HorusTemplateModule } from 'app/main/template/template.module';
import { RootDashboardModule } from 'app/main/horus/root/dashboard/dashboard.root.module';
import { CadastroDashboardModule } from './dashboard/dashboard.cadastro.module';
import { ManutencaoProdutoModule } from 'app/main/horus/cadastro/pages/CAMM15/manutencao.produto.module';
import { MaterialModule } from '../../../core/components/material.module';
import { ClientModule } from '../../../client/horus/horus.client.module';



@NgModule({

    imports: [
        TranslateModule,
        HorusTemplateModule,
        ComponentsModule,
        HorusTemplateModule,
        CadastroDashboardModule,
        ManutencaoProdutoModule,
        MaterialModule,
        ClientModule
    ],

})

export class CadastroModule {
}
