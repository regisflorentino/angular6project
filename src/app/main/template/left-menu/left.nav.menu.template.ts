import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'left-template',
  templateUrl: './left.nav.menu.template.html',
  styleUrls: ['./left.nav.menu.template.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LeftNavTemplate {

}
