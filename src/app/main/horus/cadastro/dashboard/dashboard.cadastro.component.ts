import { Component, ViewEncapsulation } from "@angular/core";
import { horusAnimation } from "../../../../core/animations";

@Component({
    templateUrl: './dashboard.cadastro.component.html',
    styleUrls: ['./dashboard.cadastro.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: horusAnimation
})
export class CadastroDashboardComponent {

}