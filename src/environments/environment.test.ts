import { EndPoint } from "app/core/client/endpoint.server";

export const environment = {
  production: false,
  ambiente: 'UNIT-TEST',
  serviceEndPoints : {
    seguranca: new EndPoint('http://localhost12', null, 8180),
    produto: new EndPoint('http://localhos12t', null, 8280),
    produtoSemVenda: new EndPoint('http://localhos12t', null, 8380),
    workflow: new EndPoint('http://localhos12t', null, 8480),
  }
};
