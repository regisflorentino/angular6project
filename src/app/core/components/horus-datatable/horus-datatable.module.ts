import { NgModule } from '@angular/core';
import { HorusDatatableComponent2 } from './horus-datatable.component';
import { CommonModule } from '@angular/common';
import { PrimefacesModule } from '../primefaces.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
    providers: [
    ],

    declarations: [
        HorusDatatableComponent2
    ],

    imports: [
        CommonModule,
        ReactiveFormsModule,
        PrimefacesModule,
    ],

    exports: [
        HorusDatatableComponent2
    ]
})

export class HorusDatatableModule2 { }
