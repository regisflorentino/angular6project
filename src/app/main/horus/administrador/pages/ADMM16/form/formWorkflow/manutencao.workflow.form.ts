import { Component, ViewEncapsulation, Injector } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { SelectItem } from "primeng/components/common/selectitem";
import { TranslateService } from "@ngx-translate/core";
import { HorusTranslationLoaderService } from "app/core/service/translation-loader.service";

import { locale as portugues } from '../../i18n/pt';
import { horusAnimation } from "app/core/animations";
import { HorusFormPage } from "../../../../../../template/typescript/form.page";
import { ActivatedRoute } from "@angular/router";

import { environment as _environment } from 'environments/environment';

@Component({
    templateUrl: './manutencao.workflow.form.html',
    styleUrls: ['./manutencao.workflow.form.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: horusAnimation
})
export class ManutencaoWorkflowFormComponent extends HorusFormPage {

    workflowNome: string;
    status: boolean;
    serviceEndPoints: any;
    modulo: any;
    listagem: any;

    listagens: Array<any>;

    modulos: Array<any>;

    constructor(protected activatedRoute: ActivatedRoute, protected injector: Injector){
        super(injector, activatedRoute, [portugues]);
        this.serviceEndPoints = _environment.serviceEndPoints;
        this.modulos = new Array<any>();

        this.modulo = {label: this.translate.instant('ADMM16.FORM.SELECIONE'), value: null};
        this.modulos.push(this.modulo);

        for(let index in this.serviceEndPoints){
            if(index.toUpperCase() !== 'WORKFLOW'){
                this.modulos.push({label: this.translate.instant('ADMM16.FORM.' + index.toUpperCase()), value: index.toUpperCase()});
            }
        }

        this.buildForm();
    }

    changeModulo(event){
        console.log(this.modulo);
    }

    private buildForm() {
        this.validationForm.init({
            workflowNome: {}
        });
        this.validationForm.buildForm({
            workflowNome: [this.workflowNome, Validators.required],
            modulo: [this.modulo, Validators.required],
            listagem: [this.listagem, Validators.required],
            status: [this.status]
        });
        this.validationForm.valuesChanges();
    }

}