import { InMemoryDbService } from 'angular-in-memory-web-api';
import { WorkflowFakeDB } from 'test/mock-db/fake-db/workflow/workflows';
import { MenuFakeDB } from './fake-db/menu/menus';
import { environment } from 'environments/environment';

export class FakeDbService implements InMemoryDbService {
    createDb() {
        let fakeDb = {};

        fakeDb['api/menu/findAll'] = MenuFakeDB.menus;
        fakeDb['api/menu/findById?id=1'] = MenuFakeDB.menus;

        return fakeDb;
    }
}
