import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HorusTemplateModule } from 'app/main/template/template.module';
import { PrimefacesModule } from '../../../../../core/components/primefaces.module';
import { ComponentsModule } from 'app/core/components/components.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DirectiveModule } from '../../../../../core/directive/diretive.module';
import { ManutencaoProdutoTabsModule } from 'app/main/horus/cadastro/pages/CAMM15/from/tabs/manutencao.produto.tabs.module';
import { MaterialModule } from 'app/core/components/material.module';
import { AuthenticatedGuard } from 'app/core/guard/authenticated.guard';
import { ClientModule } from 'app/client/horus/horus.client.module';
import { HorusFormPage } from 'app/main/template/typescript/form.page';
import { ManutencaoWorkflowFormComponent } from 'app/main/horus/administrador/pages/ADMM16/form/formWorkflow/manutencao.workflow.form';
import { ManutencaoWorkflowTabsModule } from 'app/main/horus/administrador/pages/ADMM16/form/formWorkflow/tabs/manutencao.workflow.tabs.module';


const routes: Routes = [
    {
        path: 'ADMM16/form',
        component: ManutencaoWorkflowFormComponent,
    },
];

@NgModule({
    providers: [
        
    ],

    declarations: [
        ManutencaoWorkflowFormComponent,
        
    ],

    imports: [
        ManutencaoWorkflowTabsModule,
        RouterModule.forChild(routes),
        HorusTemplateModule,
        FlexLayoutModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PrimefacesModule,
        ComponentsModule,
        DirectiveModule,
        MaterialModule,
        ClientModule
    ]
})
export class ManutencaoWorkflowModule {
}
