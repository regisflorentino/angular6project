import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrimefacesModule } from '../primefaces.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HorusDataTableComponent } from 'app/core/components/datatable/datatable.component';
import { HorusColumn } from './column/horus-column.directive';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule } from '@ngx-translate/core';
import { DataTableClientSidePipe } from 'app/core/components/datatable/filter/filter.client.side.pipe';


@NgModule({
    providers: [
        DataTableClientSidePipe
    ],

    declarations: [
        HorusDataTableComponent,
        HorusColumn,
    ],

    imports: [
        CommonModule,
        ReactiveFormsModule,
        PrimefacesModule,
        FlexLayoutModule,
        TranslateModule
    ],

    exports: [
        HorusColumn,
        HorusDataTableComponent,
    ]
})

export class HorusDatatableModule { }
