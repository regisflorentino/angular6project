import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PrimefacesModule } from '../../../../core/components/primefaces.module';
import { HorusTemplateModule } from 'app/main/template/template.module';
import { ComponentsModule } from '../../../../core/components/components.module';
import { LoginComponent } from 'app/main/horus/root/login/login.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule } from '@ngx-translate/core';
import { HorusFormModule } from 'app/core/forms/form.module';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClientModule } from 'app/client/horus/horus.client.module';

const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
    },

];

@NgModule({
    declarations: [
        LoginComponent,
    ],
    imports: [
        RouterModule.forChild(routes),
        PrimefacesModule,
        HorusTemplateModule,
        ComponentsModule,
        FlexLayoutModule,
        CommonModule,
        FormsModule, 
        ReactiveFormsModule,
        HorusFormModule,
        ClientModule
    ]
})
export class LoginModule {
}
