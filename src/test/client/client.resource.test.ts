import { HorusClient } from "app/client/horus/horus.client";
import { IClientResourceBuilder } from "app/core/client/client.resource";
import { Injectable } from "@angular/core";

export class HorusClientResourceTest implements IClientResourceBuilder {

    buildResourceUrl(client: HorusClient<any>) {
        let url = 'api'.concat('/').concat(client['path'].replace('/', ''));
        console.log('Client Url = ' + url);
        return url;
    }

}