base.dialog = function() {

	var object = {};
	object.initElement = function(element) {
		$(element).on('hide.bs.modal', hideDialog);
	}
	object.init = function() {
		$(".base-dialog").each(function(index, element) {
			object.initElement($(element));
		});

	};

	return object;
}

function setTitleDialog(dialogElement, title){
	dialogElement.find('.modal-title').text(title)
}

function showDialog(dialogElement, newUrl) {
	dialogElement.modal('show');
	var modal = $(dialogElement);
	var modalId = $(modal).attr("id");
	
	var showActionListener = 	$('#attr_' + modalId + '_showactionlistener').val();
	if(newUrl){
		showActionListener = newUrl;
	}
	var onshow = 				$('#attr_' + modalId + '_onshow').val();
	var onsuccess = 			$('#attr_' + modalId + '_onsuccess').val();
	var onerror = 				$('#attr_' + modalId + '_onerror').val();
	
	var form = modal.find('.form-dialog');

	eval(onshow);

	if(showActionListener){
		$.ajax({
			async : false,
			type : 'POST',
			url : showActionListener,
			data : form.serialize(),
			success : function(response) {
				var modalBody = $(modal.find('.modal-body'));
				modalBody.empty();
				modalBody.prepend(response);
				eval(onsuccess);
			},
			error : function(response) {
				eval(onerror);
			}
		});
	}

	$(modal.find('.modal-autocomplete')).autocomplete( "option", "appendTo", modal);		
}

function hideDialog(dialogElement) {
	var modalId = $($(dialogElement).attr("target")).attr("id");
	var onhideFuncionName = $('#attr_' + modalId + '_onhide').val();
	if(onhideFuncionName != undefined && onhideFuncionName.length > 0){
		try{
			window[onhideFuncionName].apply();
		}catch(err){
			console.log("base.dialog error - callback não definido: " + err);
		}
	}else{
		$(dialogElement).modal('hide');
	}
}

function newDialog(dialogElement, newUrl) {
	base.dialog().initElement(dialogElement);
	showDialog(dialogElement, newUrl);
}