import { NgModule, Injector } from '@angular/core';
import { CommonModule } from '@angular/common'
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { EmbalagemClient } from './embalagem/embalagem.client';
import { ProdutoDadosGeraisClient } from 'app/client/horus/produto/dadosgerais/dadosgerais.client';
import { FornecedorProdutoClient } from './fornecedor/fornecedor.client';
import { HorusResourceModule } from 'app/client/horus/resource/horus.client.resource.module';


@NgModule({
    providers: [
        EmbalagemClient,
        ProdutoDadosGeraisClient,
        FornecedorProdutoClient
    ],

    imports: [
        HttpModule,
        HorusResourceModule
    ]

})
export class ProdutoClientModule {
}
