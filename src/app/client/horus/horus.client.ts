import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";

import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { BrowserStack } from "protractor/built/driverProviders";
import { ClientParameter } from "app/client/horus/horus.client.parameter";
import { RequestOptions, Http, Headers, Response } from "@angular/http";
import { map, catchError } from 'rxjs/operators';
import { CookieService } from "ngx-cookie-service";
import { PaginationClient } from "app/client/horus/pagination.client";
import { Page } from "app/core/client/page";
import { Header } from "primeng/components/common/shared";
import { PaginationParameter } from "app/core/components/datatable/pagination/datatable.parameter";
import { environment } from "environments/environment";
import { HorusClientResource } from "app/client/horus/resource/horus.client.resource";


@Injectable()
export abstract class HorusClient<T> {

    constructor(protected http: Http, private cookieService: CookieService, private horusClientResource: HorusClientResource){
        let resource = this.horusClientResource.buildResourceUrl(this);
        this['resourceurl'] = resource;
    }

    protected headers(publicAcess: boolean, applicationType ?: string) : Headers{
        let accessToken = this.cookieService.get('access_token');
        //if(!accessToken && !publicAcess){
        //    throw new Error('AccessToken Not defined')
        //}
        let headers: Headers = new Headers({
            "Content-Type": 
                applicationType ?  
                    applicationType
                    : "application/x-www-form-urlencoded",
            "Authorization": 
                publicAcess ? 
                    "Basic aG9ydXNfdmlldzpob3J1c192aWV3X2FwcGxpY2F0aW9ucw==" 
                    : "bearer " + accessToken,
        });
        return headers;
    }

    protected post(parameter: ClientParameter){
        let url: string = this['resourceurl'] + parameter.path;
        let parametersBody = parameter.parameters;
        let options = new RequestOptions({ headers: this.headers(parameter.publicAcess, parameter.applicationType)});
        return this.http.post(url, parametersBody, options).pipe(
            map(response => this.responseMap(response, parameter.map)),
            catchError(error => parameter.handler ? parameter.handler : this.handleError(error))
        );
    }

    protected get(parameter: ClientParameter){
        let url: string = this['resourceurl'];
        if(parameter.path){
            url+=parameter.path;
        }
        if(parameter.parameters){
            url+=parameter.parameters;
        }
        
        return this.http.get(url, { headers : this.headers(parameter.publicAcess, parameter.applicationType)}).pipe(
            map(response => this.responseMap(response, parameter.map)),
            catchError(error => parameter.handler ? parameter.handler : this.handleError(error))
        );
    }

    protected delete(parameter: ClientParameter){
        let url: string = this['resourceurl'] + parameter.path;
        let parametersBody = parameter.parameters;
        let options = new RequestOptions({ headers: this.headers(parameter.publicAcess)});
        return this.http.delete(url, options).pipe(
            map(response => this.responseMap(response, parameter.map)),
            catchError(error => this.catchError(error, parameter.handler))
        );
    }

    page(parameterPage: PaginationParameter<T>):  Observable<Page<T>> {
        let parameter = {
            path: '/page?pageSize=' + parameterPage.limitPage 
                + '&pageNumber=' + parameterPage.pageNumber 
                + '&count=' + parameterPage.totalItems
                + '&sort=' + parameterPage.sortField
                + '&sortDirection=' + parameterPage.sortDirection,
            parameters: JSON.stringify(parameterPage.model),
            applicationType: 'application/json'
        }
        return this.post(parameter);
    }
    
    count(entity ?: T):  Observable<number> {
        let param : ClientParameter = {
            path: '/count',
            applicationType: 'application/json'
        }
        if(entity){
            param.parameters = JSON.stringify(entity);
        } else {
            param.parameters = {}
        }
        return this.post(param);
    }
    
    protected handleError(error: Response) {
        if (error.status === 403) {
            let result = error.json();
            if(result['error'] === 'unauthorized'){
        
            }
            throw(result);
        }
        if(error.status === 404){
            console.log('Not Fount Path');
            throw(error);
        }
        
    }
    
    protected responseMap(response: Response, map: any): any {
        try {
            return response.json();
        } catch (error) {
            if(response['_body'].length === 0){
                return null;
            }
            console.log(error);
            return response;
        }
    }

    protected catchError(response: Response, map: any): any {
        try {
            return this.handleError(response);
        } catch (error) {
            if(response['_body'].length === 0){
                return null;
            }
            return response;
        }
    }

}
