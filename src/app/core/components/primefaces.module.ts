import { NgModule } from '@angular/core';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {FieldsetModule} from 'primeng/fieldset';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ButtonModule} from 'primeng/button';
import {DropdownModule} from 'primeng/dropdown';
import {CheckboxModule} from 'primeng/checkbox';
import {TabViewModule} from 'primeng/tabview'
import { AutoCompleteModule } from 'primeng/autocomplete';
import {InputSwitchModule} from 'primeng/inputswitch';
import {TreeModule} from 'primeng/tree';
import {TableModule} from 'primeng/table';
import {TreeTableModule} from 'primeng/treetable';
import {PanelMenuModule} from 'primeng/panelmenu';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import {DialogModule} from 'primeng/dialog';


@NgModule({

    imports: [
        FieldsetModule,
        ButtonModule,
        DropdownModule,
        CheckboxModule,
        TabViewModule,
        AutoCompleteModule,
        TreeModule,
        TableModule,
        TreeTableModule,
        PanelMenuModule,
        ScrollPanelModule,
        InputSwitchModule,
        DialogModule
    ],

    exports: [
        FieldsetModule,
        ButtonModule,
        DropdownModule,
        CheckboxModule,
        TabViewModule,
        AutoCompleteModule,
        TreeModule,
        TableModule,
        TreeTableModule,
        PanelMenuModule,
        ScrollPanelModule,
        InputSwitchModule,
        DialogModule
    ]
})
export class PrimefacesModule { }
