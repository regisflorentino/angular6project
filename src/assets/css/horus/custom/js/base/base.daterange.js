base.daterange = function(){
	var object = {};
	
	object.initElement = function(element){
		
		var nameInicio = element.find("input[class='nameInicio']");
		var nameFim = element.find("input[class='nameFim']");
		var span = element.find("div[class='date-rangepicker'] span");
		var div = element.find("div[class='date-rangepicker']");
		
		var minDate = element.attr('data-min-date');
		var maxDate = element.attr('data-max-date');
		var startDate = element.attr('data-start-date');
		var endDate = element.attr('data-end-date');
		var limiteDatas = element.attr('data-limite-datas');
		if (limiteDatas == ""){
			limiteDatas = 12;
		}
		var substractYear = element.attr('data-substract-year');
		if (substractYear == ""){
			substractYear = 0;
		}
		

		element.find(".date-rangepicker").daterangepicker({
			opens: element.attr('data-opens') || 'left',
			format: 'DD/MM/YYYY',
			startDate: startDate,
			endDate: endDate,
			minDate: minDate,
			maxDate: maxDate,
			dateLimit: {months: limiteDatas},
			ranges: { 
				'Dia anterior': [ moment().subtract({days:1, years:substractYear}).startOf('day'), moment().subtract({days:1, years:substractYear}).startOf('day')],
				'Final de semana anterior': [ moment().subtract({days:(moment().weekday()+1), years:substractYear}).startOf('day'), moment().subtract({days:moment().weekday(), years:substractYear}).startOf('day')],
				'Último mês': [ moment().subtract({month:1, years:substractYear}).startOf('month'), moment().subtract({month:1, years:substractYear}).endOf('month')],
				'Último trimestre': [ moment().subtract({month:3, years: substractYear}).startOf('month'), moment().subtract({month:1, years:substractYear}).endOf('month')],
				'Acumulado do ano': [ moment().subtract({years: substractYear}).startOf('year').startOf('month'), moment().subtract({day:1, years:substractYear})]
			},
			locale: {
				applyLabel: 'Aplicar',
				cancelLabel: 'Cancelar',
				fromLabel: 'De',
				toLabel: 'Até',
				customRangeLabel: 'Período Customizado',
				daysOfWeek: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex','Sab'],
				monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
			}
		},
		
		function(start, end, label) {
			nameInicio.val(start.format("DD/MM/YYYY"));
			nameFim.val(end.format("DD/MM/YYYY"));
			span.html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
		}
		
		);
		


		span.html(div.data().daterangepicker.startDate.format("DD/MM/YYYY") + ' - ' + div.data().daterangepicker.endDate.format("DD/MM/YYYY"));
		nameInicio.val(div.data().daterangepicker.startDate.format("DD/MM/YYYY"));
		nameFim.val(div.data().daterangepicker.endDate.format("DD/MM/YYYY"));
	};
	
	object.init = function(){
		$(".base-daterangepicker").each(function(index, obj){
			object.initElement($(obj));
		});
	}
	
	return object;
};
