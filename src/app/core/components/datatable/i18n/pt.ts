export const locale = {
    lang: 'pt',
    data: {
        'DATA_TABLE' : {
            'HEADER': {
                'REGISTROS_POR_PAGINA': 'Registros Por Página',
                'PAGINA': 'Página',
                'DE' : 'de {{value}}'
            },
            'FORM': {

            },
            'ERROR' : {
                
            }
        }
    }
};
