var NOTIFICATION_UNREAD_URL;

const
TIMEOUT = 30000;

$(document).ready(
		function() {
			NOTIFICATION_UNREAD_URL = $("#navbar_actions_notifications").data(
					'unread-notifications');
			getLastUnreadNotifications();
		});

function getLastUnreadNotifications() {
	$.get(NOTIFICATION_UNREAD_URL, function(data) {
		setTimeout(getLastUnreadNotifications, TIMEOUT);
	}).done(
			function(html) {
				if ($("#navbar_actions_notifications").find('.badge-important')
						.text().length > 0) {
					var newMessages = $(html).find('.badge-important').text()
							- $("#navbar_actions_notifications").find(
									'.badge-important').text();
					if (newMessages > 0) {
						var message = 'Você tem '
								+ newMessages
								+ (newMessages === 1 ? ' nova mensagem'
										: ' novas mensagens');
						// eval("notificationRemote([{name:'newMessages', value:
						// '" + message + "'}])");
						notifyMe(message);
					}
				}
				$("#navbar_actions_notifications").html(html);

			});
};
function notificationUnreadUpdate() {
	$.get(NOTIFICATION_UNREAD_URL).done(function(html) {
		$("#navbar_actions_notifications").html(html);
	});
}

function notificationCreateEventUpdate(id) {
	var listener = document.createEvent("Event");
	listener.id = id;
	listener.initEvent("EVENT_NOTIFICATION_UPDATE", true, true);

	document.dispatchEvent(listener);
}

function notifyMe(message) {
	var notification;
	if (!("Notification" in window)) {
		alert("Você possui novas mensgens");
	} else if (Notification.permission === "granted") {
		notification = showNotification(message);
	} else if (Notification.permission !== 'denied') {
		Notification.requestPermission(function(permission) {
			if (permission === "granted") {
				notification = showNotification(message);
			}
		});
	}
}


function showNotification(body) {

	var options = {
		body : body,
		icon : 'template/base/custom/img/notification-logo.png'
	}

	var notification = new Notification('Notificação', options);

	notification.onclick = function() {
		openTabNotification(notification);
	};
	setTimeout(notification.close.bind(notification), 4000);
	return notification;
}

function openTabNotification(notification){
	var url = window.location.origin;
	url = url + '/ADM/notificationJSF';  
	notification.close();
	window.open(url,'_blank');
	window.open(url);
}