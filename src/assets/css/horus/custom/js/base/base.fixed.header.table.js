base.ftable = function() {
	var object = {};

	object.init = function() {
		
		$(document).scroll(function(){
			if($(document).scrollTop() > 0){
				$(".fheadertable-sticky-header").floatThead({scrollingTop:173});
				$(".base-ftable").find(".floatThead-wrapper").find(".floatThead-floatContainer").css("margin-top","-89px");
				$(".fheadertable-tableContent").css("margin-top","0px");
			}
		});
	}

	return object;
}
