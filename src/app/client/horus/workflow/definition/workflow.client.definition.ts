import { Injectable } from "@angular/core";
import { ClientResource } from "app/core/client/client.resource";
import { HorusClient } from "app/client/horus/horus.client";
import { environment } from "environments/environment";
import { WorkflowConfiguration } from "app/core/model/workflow/workflow.definition.model";
import { CookieService } from "ngx-cookie-service";
import { Http } from "@angular/http";

@ClientResource({
    path: '/workflow/difinition'
})
export class WorkflowDefinition extends HorusClient<WorkflowConfiguration>{


}