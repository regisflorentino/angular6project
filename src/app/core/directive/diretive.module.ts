import { NgModule } from '@angular/core';
import { MessageDirective } from 'app/core/directive/message/message.directive';
import { HorusAutocompleteDirective } from './autocomplete/autocomplete.directive';

@NgModule({

    declarations: [
        MessageDirective,
        HorusAutocompleteDirective
    ],

    exports: [
        MessageDirective,
        HorusAutocompleteDirective
    ],
})
export class DirectiveModule { }
