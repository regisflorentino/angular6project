import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PrimefacesModule } from '../../../../core/components/primefaces.module';
import { HorusTemplateModule } from 'app/main/template/template.module';
import { ComponentsModule } from '../../../../core/components/components.module';
import { CadastroDashboardComponent } from './dashboard.cadastro.component';



const routes: Routes = [
    {
        path: '',
        component: CadastroDashboardComponent,
    },

];

@NgModule({
    declarations: [
        CadastroDashboardComponent,
    ],
    imports: [
        RouterModule.forChild(routes),
        PrimefacesModule,
        HorusTemplateModule,
        ComponentsModule
    ]
})
export class CadastroDashboardModule {
}
