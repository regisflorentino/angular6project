import { Embalagem } from "./embalagem.model";
import { TransacaoContextDomainChildComponent } from "app/core/model/context/transacao.context.domain.child.model";

export interface CodigoDeBarra {

    id          : number;
    embalagem   : Embalagem;
    codigo      : string;
    tipo        : string;
    ativo       : boolean;
    transacao   : TransacaoContextDomainChildComponent;
	fgEdi       : boolean;
    fgUtilVenda : boolean;
	dataUltimaVenda : Date;
}