import { WorkflowModel } from "./workflow.model";
import { ValorAcionamentoWorkflowModel } from "app/core/model/workflow/valor.acionamento.model";

export interface AcionamentoWorkflowModel {

    id                          : number;
    workflow                    : WorkflowModel;
    tabelaAcionamento           : string;
    campoTabela                 : string;
    tipo                        : 'LIKE' | 'IN';
    referenciaTabelaAcionamento : string;
    valores                     : Array<ValorAcionamentoWorkflowModel>;

}