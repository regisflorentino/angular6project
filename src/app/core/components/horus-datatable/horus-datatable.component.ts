import { Component, Input, EventEmitter, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { PaginacaoHorus } from './domain/paginacaoHorus';
import { HttpClient } from '@angular/common/http';
import { SortEvent } from 'primeng/api';


@Component({
  selector: 'horus-datatable',
  templateUrl: './horus-datatable.component.html',
  styleUrls: ['./horus-datatable.component.scss'],
  
})
export class HorusDatatableComponent2 implements OnChanges {

  @Input() cols: any[];
  @Input() list: any[];
  @Input() listSelections: any[];
  @Input() url: string;
  @Input() rows: number;
  @Input() paginacao: PaginacaoHorus;
  noResult: string;
  temPaginaAnterior: boolean;
  results: any[];

  ngOnChanges(changes: SimpleChanges) {
    const name: SimpleChange = changes.paginacao;
    
  }

  
 
  constructor(private http: HttpClient){
    //this.noResult = "Registros não encontrados"
    //this.paginacao = { pagina: 1, registrosPorPagina: 10, totalPaginas: 1, totalRegistros: 0 };
  }

  atualizarRows(){
    this.paginacao.registrosPorPagina;
  }
  page() {
    this.buscarDados();
    console.log( 'Paginação com ' + this.paginacao.registrosPorPagina + ' registros na página ' + this.paginacao.pagina );
  }

  firstPage() {
    this.paginacao.pagina = 1;
    this.buscarDados();
    console.log( 'Paginação com ' + this.paginacao.registrosPorPagina + ' registros, e na página ' + this.paginacao.pagina  );
  }

  lastPage() {
    this.paginacao.pagina = this.paginacao.totalPaginas;
    this.buscarDados();
    console.log( 'Paginação com ' + this.paginacao.registrosPorPagina + ' registros, e na página ' + this.paginacao.pagina  );
  }

  previousPage() {
    if (this.hasPrevious()){
      this.paginacao.pagina = (this.paginacao.pagina - 1);
      this.buscarDados();
      console.log( 'Paginação com ' + this.paginacao.registrosPorPagina + ' registros, e na página ' + this.paginacao.pagina  );
    }
  }

  nextPage() {
    if (this.hasNext()){
      this.paginacao.pagina = (this.paginacao.pagina + 1);
      this.buscarDados();
      console.log( 'Paginação com ' + this.paginacao.registrosPorPagina + ' registros, e na página ' + this.paginacao.pagina  );
    }
  }
  
  hasPrevious(): boolean { return +this.paginacao.pagina > 1; }
  
  hasNext(): boolean { return +this.paginacao.pagina < +this.paginacao.totalPaginas; }
  
  private buscarDados() {
    console.warn('DADOS MOCADOS')
    this.http.get<any>(this.url + '.' + this.paginacao.pagina + '.json')
      .toPromise()
      .then(data => this.list = data);
  }
  customSort(event: SortEvent) {

    event.data.sort((data1, data2) => {
        let value1 = data1[event.field];
        let value2 = data2[event.field];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order * result);
    });
}




}
