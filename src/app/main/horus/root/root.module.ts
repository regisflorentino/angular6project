import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from '../../../core/components/components.module';
import { HorusTemplateModule } from 'app/main/template/template.module';
import { RootDashboardModule } from 'app/main/horus/root/dashboard/dashboard.root.module';
import { LoginModule } from 'app/main/horus/root/login/login.module';



@NgModule({

    imports: [
        TranslateModule,
        HorusTemplateModule,
        ComponentsModule,
        HorusTemplateModule,
        RootDashboardModule,
        LoginModule
    ],

})

export class RootModule {
}
